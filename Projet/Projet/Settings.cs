﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Projet {
    /// <summary>
    /// contient un Singleton (3 paramètres), et des attributs statiques
    /// </summary>
    [Serializable()]
    [XmlRoot("settings")]
    public class Settings {

        /// <summary>
        /// Instance du Singleton
        /// </summary>
        private static Settings instance;

        // public static readonly string APPDATAPATH = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\multiplikator2000\";
        // public static readonly string PROFILESPATH = APPDATAPATH + @"profiles\";

        /// <summary>
        ///     Nombre d'états transition
        ///     Par exemple, si nbTransitionState = 3, chaque niveau a un total de 4 états
        /// </summary>
        [XmlElement("nbTransitionState")]
        public int nbTransitionState;

        /// <summary>
        ///     Score minimal pour considérer un succès
        /// </summary>
        [XmlElement("minimumScore")]
        public int minimumScore;

        /// <summary>
        ///     Nombre de succès consécutifs pour passer à la transition précédente ou suivante
        /// </summary>
        [XmlElement("nbSuccess")]
        public int nbSuccess;

        /// <summary>
        ///     Noms des différents niveaux de profile
        /// </summary>
        [XmlArray("levelsNames")]
        [XmlArrayItem(ElementName = "levelName")]
        public List<string> levelsNames;

        private Settings() { }

        /// <summary>
        /// Charger les paramètres depuis le fichier settings.xml
        /// </summary>
        public void loadSettings() {
            XmlSerializer serializer = new XmlSerializer(typeof(Settings));
            // File exists ?
            if (File.Exists(APPDATAPATH + "settings.xml")) {
                StreamReader reader = new StreamReader(APPDATAPATH + "settings.xml");
                Settings deser = (Settings)serializer.Deserialize(reader);
                Settings.instance = deser;
                reader.Close();
            }
            else {
                // Default values
                this.nbSuccess = 3;
                this.nbTransitionState = 3;
                this.minimumScore = 17;
                this.levelsNames = new List<string> { "LEVEL1_1", "LEVEL1_2", "LEVEL1_3", "LEVEL2", "LEVEL3" };
                
                //levelsNames = new List<int> { 0, 1, 2 };
                Settings.instance.saveSettings();
            }
        }

        /// <summary>
        /// Sauvegarder les paramètres dans le fichier settings.xml
        /// </summary>
        public void saveSettings() {
            XmlSerializer serializer = new XmlSerializer(typeof(Settings));
            StreamWriter writer = new StreamWriter(APPDATAPATH + "settings.xml");
            serializer.Serialize(writer, Settings.instance);
            writer.Close();
        }

        /// <summary>
        /// Récupère l'instance du Singleton
        /// </summary>
        public static Settings Instance {
            get {
                if (instance == null) {
                    instance = new Settings();
                }
                return instance;
            }
        }

        /// <summary>
        /// Readonly : Chemin vers les fichiers locaux de l'application (contient un "\" final)
        /// </summary>
        public static string APPDATAPATH {
            get {
                return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\multiplikator2000\";
            }
        }

        /// <summary>
        /// Readonly : Chemin vers le dossier de profils (contient un "\" final)
        /// </summary>
        public static string PROFILESPATH {
            get {
                return APPDATAPATH + @"profiles\";
            }
        }
    }
}