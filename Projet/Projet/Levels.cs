﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet {
    /// <summary>
    /// Les différents niveaux possible
    /// </summary>
    public enum Levels { LEVEL1_1, LEVEL1_2, LEVEL1_3, LEVEL2, LEVEL3 };

    public static class Level {

        /// <summary>
        /// Renvoie le nom du niveau (choisi dans les paramètres)
        /// </summary>
        /// <returns></returns>
        public static String toString(this Levels lvl) {
            switch (lvl) {
                case Levels.LEVEL1_1:
                    return Settings.Instance.levelsNames[0];
                case Levels.LEVEL1_2:
                    return Settings.Instance.levelsNames[1];
                case Levels.LEVEL1_3:
                    return Settings.Instance.levelsNames[2];
                case Levels.LEVEL2:
                    return Settings.Instance.levelsNames[3];
                case Levels.LEVEL3:
                    return Settings.Instance.levelsNames[4];
                default:
                    return "";
            }
        }

        /// <summary>
        /// Renvoie l'entier lié au niveau
        /// </summary>
        /// <returns></returns>
        public static int getIntegerId(this Levels lvl) {
            switch (lvl) {
                case Levels.LEVEL1_1: return 0;
                case Levels.LEVEL1_2: return 1;
                case Levels.LEVEL1_3: return 2;
                case Levels.LEVEL2: return 3;
                case Levels.LEVEL3: return 4;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Permet de récupérer un niveau à partir de l'entier correspondant
        /// </summary>
        /// <param name="integer"></param>
        /// <returns></returns>
        public static Levels getLevelFromInt(int integer) {
            switch (integer) {
                case 0: return Levels.LEVEL1_1;
                case 1: return Levels.LEVEL1_2;
                case 2: return Levels.LEVEL1_3;
                case 3: return Levels.LEVEL2;
                case 4: return Levels.LEVEL3;
                default: return Levels.LEVEL1_1;
            }
        }

        /// <summary>
        /// Renvoie le minimum possible pour l'opérande gauche d'une opération
        /// </summary>
        /// <param name="lvl">Niveau de l'opération</param>
        /// <returns></returns>
        public static int getMinLeft(this Levels lvl) {
            switch (lvl) {
                case Levels.LEVEL1_1:
                    return 1;
                case Levels.LEVEL1_2:
                    return 4;
                case Levels.LEVEL1_3:
                    return 7;
                default:
                    return 10;
            }
        }

        /// <summary>
        /// Renvoie le maximum possible pour l'opérande gauche d'une opération
        /// </summary>
        /// <param name="lvl">Niveau de l'opération</param>
        /// <returns></returns>
        public static int getMaxLeft(this Levels lvl) {
            switch (lvl) {
                case Levels.LEVEL1_1:
                    return 3;
                case Levels.LEVEL1_2:
                    return 6;
                case Levels.LEVEL1_3:
                    return 9;
                default:
                    return 19;
            }
        }

        /// <summary>
        /// Renvoie le minimum possible pour l'opérande droite d'une opération
        /// (Correspond au minimum de l'opérande gauche)
        /// </summary>
        /// <param name="lvl">Niveau de l'opération</param>
        /// <returns></returns>
        public static int getMinRight(this Levels lvl) {
            return lvl.getMinLeft();
        }

        /// <summary>
        /// Renvoie le maximum possible pour l'opérande droite d'une opération
        /// (Renvoie 9 pour les 3 premiers niveaux, et 19 pour les autres)
        /// </summary>
        /// <param name="lvl">Niveau de l'opération</param>
        /// <returns></returns>
        public static int getMaxRight(this Levels lvl) {
            if (lvl == Levels.LEVEL1_1 || lvl == Levels.LEVEL1_2 || lvl == Levels.LEVEL1_3) {
                return 9;
            }
            return 19;
        }
    }

    public class LevelComboboxItem {

        private Levels _level;

        public LevelComboboxItem(Levels level) {
            this._level = level;
        }

        public string Text {
            get {
                return this._level.toString();
            }
        }
        public Levels Value {
            get {
                return this._level;
            }
        }

        public override string ToString() {
            return Text;
        }
    }
}