﻿using System;
using System.Windows.Forms;

namespace Projet.Panels {
    /// <summary>
    /// Panneau de connexion à l'application
    /// </summary>
    public partial class LoginProfilePanel : UserControl, PanelInterface {
        public LoginProfilePanel() {
            InitializeComponent();
        }

        public void cleanPanel() { }

        // ----- Events -----

        /// <summary>
        /// Redimentionnement du panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loginProfilePanel_Resize(object sender, EventArgs e) {
            this.loginGrb.Left = (this.ClientSize.Width - this.loginGrb.Width) / 2;
            this.loginGrb.Top = (this.ClientSize.Height - this.loginGrb.Height) / 4;
        }
    }
}
