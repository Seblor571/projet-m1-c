﻿using System;
using System.Windows.Forms;

namespace Projet.Panels {
    /// <summary>
    /// Panneau d'administration
    /// </summary>
    public partial class AdminPanel : UserControl, PanelInterface {

        /// <summary>
        /// Évènement de la demande d'affichage de résultats
        /// </summary>
        public event showStatsHandler showStats;

        /// <summary>
        /// Délégué de la demande d'affichage de résultats
        /// </summary>
        public delegate void showStatsHandler(Profile profile);

        public AdminPanel() {
            InitializeComponent();
            enableAppSettings(Session.Instance.isAdmin);
        }

        // ----- GUI functions -----

        public void cleanPanel() {
            emptyProfileSetting();
            loadSettings();
        }

        /// <summary>
        /// Chargement des paramètres
        /// </summary>
        internal void loadSettings() {
            this.minScoreNum.Value = Settings.Instance.minimumScore;
            this.nbSuccessNum.Value = Settings.Instance.nbSuccess;
            this.nbTransitionNum.Value = Settings.Instance.nbTransitionState;
            this.transitionTrb.Maximum = Settings.Instance.nbTransitionState;
            this.lvl1Txt.Text = Settings.Instance.levelsNames[0];
            this.lvl2Txt.Text = Settings.Instance.levelsNames[1];
            this.lvl3Txt.Text = Settings.Instance.levelsNames[2];
            this.lvl4Txt.Text = Settings.Instance.levelsNames[3];
            this.lvl5Txt.Text = Settings.Instance.levelsNames[4];
        }


        /// <summary>
        /// (Dés)Active les éléments de modification des options de l'application
        /// </summary>
        /// <param name="enable"></param>
        private void enableAppSettings(bool enable) {
            this.minScoreNum.Enabled = enable;
            this.nbSuccessNum.Enabled = enable;
            this.nbTransitionNum.Enabled = enable;
        }

        /// <summary>
        /// Réinitialiser les éléments de modification de profil
        /// </summary>
        private void emptyProfileSetting() {
            this.usernameTxt.Text = "";
            this.firstnameTxt.Text = "";
            this.nameTxt.Text = "";
            this.levelCbb.SelectedIndex = -1;
            this.levelCbb.Text = "";
            this.transitionTrb.Value = 0;
            this.transitionNum.Value = 0;
            this.evolvingChb.Checked = false;
            this.resultsBtn.Enabled = false;
            this.loginsCbb.SelectedIndex = -1;
            this.loginsCbb.Text = "";
            enableProfileSettings(false);
        }

        /// <summary>
        /// (Dés)Active les éléments de modification de profil
        /// </summary>
        /// <param name="enable"></param>
        private void enableProfileSettings(bool enable) {
            this.firstnameTxt.Enabled = enable;
            this.nameTxt.Enabled = enable;
            this.levelCbb.Enabled = enable;
            this.transitionTrb.Enabled = enable;
            this.transitionNum.Enabled = enable;
            this.evolvingChb.Enabled = enable;
            this.saveProfileBtn.Enabled = enable;
            this.addPlayTestBtn.Enabled = enable;
        }

        // ----- Events -----

        /// <summary>
        /// Chagement du profil sélectionné
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loginsCbb_SelectedIndexChanged(object sender, EventArgs e) {
            if (this.loginsCbb.SelectedItem == null) {
                emptyProfileSetting();
                return;
            }
            // Activate result button
            this.resultsBtn.Enabled = true;

            // Activate profile settings if Admin session
            enableProfileSettings(Session.Instance.isAdmin);

            // Chosen profile
            Profile pro = ((ProfileComboBoxItem)this.loginsCbb.SelectedItem).Value;

            this.usernameTxt.Text = pro._username;
            this.firstnameTxt.Text = pro._firstname;
            this.nameTxt.Text = pro._name;
            this.levelCbb.Text = pro._level.toString();
            this.levelCbb.SelectedIndex = pro._level.getIntegerId();
            this.transitionTrb.Value = pro._transition;
            this.transitionNum.Value = pro._transition;
            this.evolvingChb.Checked = pro._evolving;

            bool lastLevel = (pro._level == Levels.LEVEL3);
            this.transitionTrb.Enabled = !lastLevel && Session.Instance.isAdmin;
            this.transitionNum.Enabled = !lastLevel && Session.Instance.isAdmin;
        }

        /// <summary>
        /// Redimentionnement du panel des paramètres de l'application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void changeSettingsGrb_Resize(object sender, EventArgs e) {
            this.settingsGrb.Left = (this.ClientSize.Width - this.settingsGrb.Width) / 2;
            this.settingsGrb.Top = (this.ClientSize.Height - this.settingsGrb.Height) / 8;
        }

        /// <summary>
        /// Sauvegarder les paramètres de l'application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveSettingsBtn_Click(object sender, EventArgs e) {
            Settings.Instance.minimumScore = Convert.ToInt32(this.minScoreNum.Value);
            Settings.Instance.nbSuccess = Convert.ToInt32(this.nbSuccessNum.Value);
            Settings.Instance.nbTransitionState = Convert.ToInt32(this.nbTransitionNum.Value);
            Settings.Instance.levelsNames[0] = this.lvl1Txt.Text;
            Settings.Instance.levelsNames[1] = this.lvl2Txt.Text;
            Settings.Instance.levelsNames[2] = this.lvl3Txt.Text;
            Settings.Instance.levelsNames[3] = this.lvl4Txt.Text;
            Settings.Instance.levelsNames[4] = this.lvl5Txt.Text;
            Settings.Instance.saveSettings();

            // Mise à jour des composants de paramètres
            this.transitionTrb.Maximum = Settings.Instance.nbTransitionState;
            this.transitionNum.Maximum = Settings.Instance.nbTransitionState;

            this.levelCbb.Items.Clear();
            foreach (Levels level in Enum.GetValues(typeof(Levels))) {
                this.levelCbb.Items.Add(new LevelComboboxItem(level));
            }
        }

        /// <summary>
        /// Synchronisation avec la trackbar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void transitionNum_ValueChanged(object sender, EventArgs e) {
            this.transitionTrb.Value = Convert.ToInt32(this.transitionNum.Value);
        }

        /// <summary>
        /// Synchronisation avec le NumericUpDown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void transitionTrb_Scroll(object sender, EventArgs e) {
            this.transitionNum.Value = this.transitionTrb.Value;
        }


        /// <summary>
        /// Redimentionnement du panel des options de profil
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chooseProfileGrb_Resize(object sender, EventArgs e) {
            this.profileOptionsGrb.Left = (this.ClientSize.Width - this.profileOptionsGrb.Width) / 2;
            this.profileOptionsGrb.Top = (this.ClientSize.Height - this.profileOptionsGrb.Height) / 8 + 25;
        }

        /// <summary>
        /// Sauvegarder le profil sélectionné
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveProfileSettings_Click(object sender, EventArgs e) {
            // Chosen profile
            Profile pro = ((ProfileComboBoxItem)this.loginsCbb.SelectedItem).Value;

            pro._firstname = this.firstnameTxt.Text;
            pro._name = this.nameTxt.Text;
            pro._level = ((LevelComboboxItem)this.levelCbb.SelectedItem).Value;
            pro._transition = this.transitionTrb.Value;
            pro._evolving = this.evolvingChb.Checked;
            pro.saveProfile();
        }

        /// <summary>
        /// Désactiver le changement de transition si le niveau du profil sélectionné est le maximum
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void levelCbb_SelectedIndexChanged(object sender, EventArgs e) {
            if (!Session.Instance.isAdmin | this.levelCbb.SelectedIndex == -1) {
                return;
            }
            bool lastLevel = ((LevelComboboxItem)this.levelCbb.SelectedItem).Value == Levels.LEVEL3;
            this.transitionTrb.Enabled = !lastLevel;
            this.transitionNum.Enabled = !lastLevel;

            if (lastLevel) {
                this.transitionTrb.Value = 0;
                this.transitionNum.Value = 0;
            }
        }

        /// <summary>
        /// EventHandler du click sur la demande d'affichage des résultats d'un profile
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void resultsBtn_Click(object sender, EventArgs e) {
            this.showStats?.Invoke(((ProfileComboBoxItem)this.loginsCbb.SelectedItem).Value);
        }


        /// <summary>
        /// EventHandler du click sur l'ajout d'un je ude test sur l'utilisateur sélectionné
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addPlayTestBtn_Click(object sender, EventArgs e) {
            // Chosen profile
            Profile pro = ((ProfileComboBoxItem)this.loginsCbb.SelectedItem).Value;
            
            this.levelCbb.SelectedIndex = 3;
            this.transitionTrb.Value = 0;

            pro._firstname = this.firstnameTxt.Text;
            pro._name = this.nameTxt.Text;
            pro._level = Levels.LEVEL2;
            pro._transition = 0;
            pro._evolving = this.evolvingChb.Checked;
            pro.saveProfile();

            Database.addPlayTest(pro._username);

        }
    }
}
