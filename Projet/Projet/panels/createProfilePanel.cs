﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Projet.Panels {
    /// <summary>
    /// Panneau de création de profil
    /// </summary>
    public partial class CreateProfilePanel : UserControl, PanelInterface {

        public CreateProfilePanel() {
            InitializeComponent();
        }

        public void cleanPanel() {
            this.usernameTxt.Clear();
            this.firstnameTxt.Clear();
            this.nameTxt.Clear();
            this.passTxt.Clear();
            this.passVerifyTxt.Clear();
            this.transitionTrb.Value = 0;
            this.transitionTrb.Maximum = Settings.Instance.nbTransitionState;
            this.transitionNum.Value = 0;
            this.evolvingChb.Checked = false;
        }

        // ----- Events -----

        /// <summary>
        /// Redimentionnement du panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createProfilePanel_Resize(object sender, EventArgs e) {
            this.createProfileGrb.Left = (this.ClientSize.Width - this.createProfileGrb.Width) / 2;
            this.createProfileGrb.Top = (this.ClientSize.Height - this.createProfileGrb.Height) / 4;
        }

        /// <summary>
        /// Vérifie que le mot de passe soit identique dans les 2 TextBox, ainsi que les éléments soient remplis
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void elementChanged(object sender, EventArgs e) {
            enableCreatingButton();
        }

        /// <summary>
        /// Désactiver le changement de transition si le niveau du profil sélectionné est le maximum
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void levelCbb_SelectedIndexChanged(object sender, EventArgs e) {
            if (!UserType.isMeta(Session.Instance.profile)) {
                return;
            }
            bool lastLevel = ((LevelComboboxItem)this.levelCbb.SelectedItem).Value == Levels.LEVEL3;
            this.transitionTrb.Enabled = !lastLevel;
            this.transitionNum.Enabled = !lastLevel;

            if (lastLevel) {
                this.transitionTrb.Value = 0;
                this.transitionNum.Value = 0;
            }

            this.enableCreatingButton();
        }

        /// <summary>
        /// Tente de créé un Profile, et renvoie True si création réussie
        /// </summary>
        /// <returns>Booléen : création réussie</returns>
        public bool validateProfileCreation() {
            if (Tools.profileExists(this.usernameTxt.Text)) {
                MessageBox.Show("Un profil existe déjà avec ce nom d'utilisateur.", "Profil invalide", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (this.usernameTxt.Text == "" || !Tools.validateUsername(this.usernameTxt.Text)) {
                MessageBox.Show("Ce nom de profil n'est pas valide.", "Profil invalide", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else {
                Profile created = new Profile(this.usernameTxt.Text,
                    this.passTxt.Text,
                    this.nameTxt.Text,
                    this.firstnameTxt.Text,
                    ((LevelComboboxItem)this.levelCbb.SelectedItem).Value,
                    Convert.ToInt32(this.transitionNum.Value),
                    this.evolvingChb.Checked);
                Tools.Save(created, Settings.PROFILESPATH + this.usernameTxt.Text + ".xml");
                return true;
            }
        }

        /// <summary>
        /// Synchronisation avec la trackbar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void transitionNum_ValueChanged(object sender, EventArgs e) {
            this.transitionTrb.Value = Convert.ToInt32(this.transitionNum.Value);
        }

        /// <summary>
        /// Synchronisation avec le NumericUpDown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void transitionTrb_Scroll(object sender, EventArgs e) {
            this.transitionNum.Value = this.transitionTrb.Value;
        }

        /// <summary>
        /// Évalue différents champs pour s'assurer que tout est remplis
        /// </summary>
        public void enableCreatingButton() {
            if (this.passTxt.Text == this.passVerifyTxt.Text)
            {
                this.passVerifyTxt.ForeColor = System.Drawing.Color.Black;
                if (this.passTxt.Text != ""
                && this.usernameTxt.Text != ""
                && this.firstnameTxt.Text != ""
                && this.nameTxt.Text != ""
                && this.levelCbb.SelectedIndex != -1) {
                    this.createProfileBtn.Enabled = true;
                }
                else {
                    this.createProfileBtn.Enabled = false;
                }
            }
            else {
                this.passVerifyTxt.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
}
