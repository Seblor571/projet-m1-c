﻿namespace Projet.Panels {
    partial class AdminPanel {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.loginsCbb = new System.Windows.Forms.ComboBox();
            this.loginLbl = new System.Windows.Forms.Label();
            this.manageProfileGrb = new System.Windows.Forms.GroupBox();
            this.profileOptionsGrb = new System.Windows.Forms.GroupBox();
            this.resultsBtn = new System.Windows.Forms.Button();
            this.saveProfileBtn = new System.Windows.Forms.Button();
            this.levelLbl = new System.Windows.Forms.Label();
            this.firstnameLbl = new System.Windows.Forms.Label();
            this.nameLbl = new System.Windows.Forms.Label();
            this.usernameLbl = new System.Windows.Forms.Label();
            this.transitionLbl = new System.Windows.Forms.Label();
            this.transitionNum = new System.Windows.Forms.NumericUpDown();
            this.evolvingChb = new System.Windows.Forms.CheckBox();
            this.transitionTrb = new System.Windows.Forms.TrackBar();
            this.levelCbb = new System.Windows.Forms.ComboBox();
            this.firstnameTxt = new System.Windows.Forms.TextBox();
            this.nameTxt = new System.Windows.Forms.TextBox();
            this.usernameTxt = new System.Windows.Forms.TextBox();
            this.changeSettingsGrb = new System.Windows.Forms.GroupBox();
            this.settingsGrb = new System.Windows.Forms.GroupBox();
            this.lvl5Txt = new System.Windows.Forms.TextBox();
            this.lvl5Lbl = new System.Windows.Forms.Label();
            this.lvl4Txt = new System.Windows.Forms.TextBox();
            this.lvl4Lbl = new System.Windows.Forms.Label();
            this.lvl3Txt = new System.Windows.Forms.TextBox();
            this.lvl3Lbl = new System.Windows.Forms.Label();
            this.lvl2Txt = new System.Windows.Forms.TextBox();
            this.lvl2Lbl = new System.Windows.Forms.Label();
            this.lvl1Txt = new System.Windows.Forms.TextBox();
            this.lvl1Lbl = new System.Windows.Forms.Label();
            this.saveSettingsBtn = new System.Windows.Forms.Button();
            this.nbSuccessLbl = new System.Windows.Forms.Label();
            this.nbTransitionLbl = new System.Windows.Forms.Label();
            this.minScoreNum = new System.Windows.Forms.NumericUpDown();
            this.minScoreLbl = new System.Windows.Forms.Label();
            this.nbSuccessNum = new System.Windows.Forms.NumericUpDown();
            this.nbTransitionNum = new System.Windows.Forms.NumericUpDown();
            this.adminSpl = new System.Windows.Forms.SplitContainer();
            this.addPlayTestBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.manageProfileGrb.SuspendLayout();
            this.profileOptionsGrb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transitionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transitionTrb)).BeginInit();
            this.changeSettingsGrb.SuspendLayout();
            this.settingsGrb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minScoreNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbSuccessNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbTransitionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adminSpl)).BeginInit();
            this.adminSpl.Panel1.SuspendLayout();
            this.adminSpl.Panel2.SuspendLayout();
            this.adminSpl.SuspendLayout();
            this.SuspendLayout();
            // 
            // loginsCbb
            // 
            this.loginsCbb.FormattingEnabled = true;
            this.loginsCbb.Location = new System.Drawing.Point(60, 28);
            this.loginsCbb.Name = "loginsCbb";
            this.loginsCbb.Size = new System.Drawing.Size(153, 21);
            this.loginsCbb.TabIndex = 7;
            this.loginsCbb.SelectedIndexChanged += new System.EventHandler(this.loginsCbb_SelectedIndexChanged);
            // 
            // loginLbl
            // 
            this.loginLbl.AutoSize = true;
            this.loginLbl.Location = new System.Drawing.Point(24, 31);
            this.loginLbl.Name = "loginLbl";
            this.loginLbl.Size = new System.Drawing.Size(30, 13);
            this.loginLbl.TabIndex = 6;
            this.loginLbl.Text = "Profil";
            // 
            // manageProfileGrb
            // 
            this.manageProfileGrb.Controls.Add(this.label1);
            this.manageProfileGrb.Controls.Add(this.addPlayTestBtn);
            this.manageProfileGrb.Controls.Add(this.profileOptionsGrb);
            this.manageProfileGrb.Controls.Add(this.loginsCbb);
            this.manageProfileGrb.Controls.Add(this.loginLbl);
            this.manageProfileGrb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manageProfileGrb.Location = new System.Drawing.Point(0, 0);
            this.manageProfileGrb.Name = "manageProfileGrb";
            this.manageProfileGrb.Size = new System.Drawing.Size(579, 255);
            this.manageProfileGrb.TabIndex = 8;
            this.manageProfileGrb.TabStop = false;
            this.manageProfileGrb.Text = "Choisir un profil à gérer";
            this.manageProfileGrb.Resize += new System.EventHandler(this.chooseProfileGrb_Resize);
            // 
            // profileOptionsGrb
            // 
            this.profileOptionsGrb.Controls.Add(this.resultsBtn);
            this.profileOptionsGrb.Controls.Add(this.saveProfileBtn);
            this.profileOptionsGrb.Controls.Add(this.levelLbl);
            this.profileOptionsGrb.Controls.Add(this.firstnameLbl);
            this.profileOptionsGrb.Controls.Add(this.nameLbl);
            this.profileOptionsGrb.Controls.Add(this.usernameLbl);
            this.profileOptionsGrb.Controls.Add(this.transitionLbl);
            this.profileOptionsGrb.Controls.Add(this.transitionNum);
            this.profileOptionsGrb.Controls.Add(this.evolvingChb);
            this.profileOptionsGrb.Controls.Add(this.transitionTrb);
            this.profileOptionsGrb.Controls.Add(this.levelCbb);
            this.profileOptionsGrb.Controls.Add(this.firstnameTxt);
            this.profileOptionsGrb.Controls.Add(this.nameTxt);
            this.profileOptionsGrb.Controls.Add(this.usernameTxt);
            this.profileOptionsGrb.Location = new System.Drawing.Point(27, 63);
            this.profileOptionsGrb.Name = "profileOptionsGrb";
            this.profileOptionsGrb.Size = new System.Drawing.Size(531, 168);
            this.profileOptionsGrb.TabIndex = 8;
            this.profileOptionsGrb.TabStop = false;
            this.profileOptionsGrb.Text = "Options du profil";
            // 
            // resultsBtn
            // 
            this.resultsBtn.Enabled = false;
            this.resultsBtn.Location = new System.Drawing.Point(6, 139);
            this.resultsBtn.Name = "resultsBtn";
            this.resultsBtn.Size = new System.Drawing.Size(75, 23);
            this.resultsBtn.TabIndex = 13;
            this.resultsBtn.Text = "Résultats";
            this.resultsBtn.UseVisualStyleBackColor = true;
            this.resultsBtn.Click += new System.EventHandler(this.resultsBtn_Click);
            // 
            // saveProfileBtn
            // 
            this.saveProfileBtn.Enabled = false;
            this.saveProfileBtn.Location = new System.Drawing.Point(450, 139);
            this.saveProfileBtn.Name = "saveProfileBtn";
            this.saveProfileBtn.Size = new System.Drawing.Size(75, 23);
            this.saveProfileBtn.TabIndex = 12;
            this.saveProfileBtn.Text = "Sauvegarder";
            this.saveProfileBtn.UseVisualStyleBackColor = true;
            this.saveProfileBtn.Click += new System.EventHandler(this.saveProfileSettings_Click);
            // 
            // levelLbl
            // 
            this.levelLbl.AutoSize = true;
            this.levelLbl.Location = new System.Drawing.Point(275, 34);
            this.levelLbl.Name = "levelLbl";
            this.levelLbl.Size = new System.Drawing.Size(41, 13);
            this.levelLbl.TabIndex = 11;
            this.levelLbl.Text = "Niveau";
            // 
            // firstnameLbl
            // 
            this.firstnameLbl.AutoSize = true;
            this.firstnameLbl.Location = new System.Drawing.Point(51, 86);
            this.firstnameLbl.Name = "firstnameLbl";
            this.firstnameLbl.Size = new System.Drawing.Size(43, 13);
            this.firstnameLbl.TabIndex = 10;
            this.firstnameLbl.Text = "Prénom";
            // 
            // nameLbl
            // 
            this.nameLbl.AutoSize = true;
            this.nameLbl.Location = new System.Drawing.Point(65, 60);
            this.nameLbl.Name = "nameLbl";
            this.nameLbl.Size = new System.Drawing.Size(29, 13);
            this.nameLbl.TabIndex = 9;
            this.nameLbl.Text = "Nom";
            // 
            // usernameLbl
            // 
            this.usernameLbl.AutoSize = true;
            this.usernameLbl.Location = new System.Drawing.Point(10, 34);
            this.usernameLbl.Name = "usernameLbl";
            this.usernameLbl.Size = new System.Drawing.Size(84, 13);
            this.usernameLbl.TabIndex = 8;
            this.usernameLbl.Text = "Nom d\'utilisateur";
            // 
            // transitionLbl
            // 
            this.transitionLbl.AutoSize = true;
            this.transitionLbl.Location = new System.Drawing.Point(265, 62);
            this.transitionLbl.Name = "transitionLbl";
            this.transitionLbl.Size = new System.Drawing.Size(53, 13);
            this.transitionLbl.TabIndex = 7;
            this.transitionLbl.Text = "Transition";
            // 
            // transitionNum
            // 
            this.transitionNum.Enabled = false;
            this.transitionNum.Location = new System.Drawing.Point(465, 60);
            this.transitionNum.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.transitionNum.Name = "transitionNum";
            this.transitionNum.Size = new System.Drawing.Size(50, 20);
            this.transitionNum.TabIndex = 6;
            this.transitionNum.ValueChanged += new System.EventHandler(this.transitionNum_ValueChanged);
            // 
            // evolvingChb
            // 
            this.evolvingChb.AutoSize = true;
            this.evolvingChb.Enabled = false;
            this.evolvingChb.Location = new System.Drawing.Point(324, 111);
            this.evolvingChb.Name = "evolvingChb";
            this.evolvingChb.Size = new System.Drawing.Size(59, 17);
            this.evolvingChb.TabIndex = 5;
            this.evolvingChb.Text = "Evolue";
            this.evolvingChb.UseVisualStyleBackColor = true;
            // 
            // transitionTrb
            // 
            this.transitionTrb.Enabled = false;
            this.transitionTrb.LargeChange = 1;
            this.transitionTrb.Location = new System.Drawing.Point(324, 60);
            this.transitionTrb.Maximum = 3;
            this.transitionTrb.Name = "transitionTrb";
            this.transitionTrb.Size = new System.Drawing.Size(135, 45);
            this.transitionTrb.TabIndex = 4;
            this.transitionTrb.Scroll += new System.EventHandler(this.transitionTrb_Scroll);
            // 
            // levelCbb
            // 
            this.levelCbb.Enabled = false;
            this.levelCbb.FormattingEnabled = true;
            this.levelCbb.Location = new System.Drawing.Point(324, 31);
            this.levelCbb.Name = "levelCbb";
            this.levelCbb.Size = new System.Drawing.Size(121, 21);
            this.levelCbb.TabIndex = 3;
            this.levelCbb.SelectedIndexChanged += new System.EventHandler(this.levelCbb_SelectedIndexChanged);
            // 
            // firstnameTxt
            // 
            this.firstnameTxt.Enabled = false;
            this.firstnameTxt.Location = new System.Drawing.Point(100, 83);
            this.firstnameTxt.Name = "firstnameTxt";
            this.firstnameTxt.Size = new System.Drawing.Size(100, 20);
            this.firstnameTxt.TabIndex = 2;
            // 
            // nameTxt
            // 
            this.nameTxt.Enabled = false;
            this.nameTxt.Location = new System.Drawing.Point(100, 57);
            this.nameTxt.Name = "nameTxt";
            this.nameTxt.Size = new System.Drawing.Size(100, 20);
            this.nameTxt.TabIndex = 1;
            // 
            // usernameTxt
            // 
            this.usernameTxt.Enabled = false;
            this.usernameTxt.Location = new System.Drawing.Point(100, 31);
            this.usernameTxt.Name = "usernameTxt";
            this.usernameTxt.Size = new System.Drawing.Size(100, 20);
            this.usernameTxt.TabIndex = 0;
            // 
            // changeSettingsGrb
            // 
            this.changeSettingsGrb.Controls.Add(this.settingsGrb);
            this.changeSettingsGrb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.changeSettingsGrb.Location = new System.Drawing.Point(0, 0);
            this.changeSettingsGrb.Name = "changeSettingsGrb";
            this.changeSettingsGrb.Size = new System.Drawing.Size(579, 258);
            this.changeSettingsGrb.TabIndex = 9;
            this.changeSettingsGrb.TabStop = false;
            this.changeSettingsGrb.Text = "Changer les paramètres";
            this.changeSettingsGrb.Resize += new System.EventHandler(this.changeSettingsGrb_Resize);
            // 
            // settingsGrb
            // 
            this.settingsGrb.Controls.Add(this.lvl5Txt);
            this.settingsGrb.Controls.Add(this.lvl5Lbl);
            this.settingsGrb.Controls.Add(this.lvl4Txt);
            this.settingsGrb.Controls.Add(this.lvl4Lbl);
            this.settingsGrb.Controls.Add(this.lvl3Txt);
            this.settingsGrb.Controls.Add(this.lvl3Lbl);
            this.settingsGrb.Controls.Add(this.lvl2Txt);
            this.settingsGrb.Controls.Add(this.lvl2Lbl);
            this.settingsGrb.Controls.Add(this.lvl1Txt);
            this.settingsGrb.Controls.Add(this.lvl1Lbl);
            this.settingsGrb.Controls.Add(this.saveSettingsBtn);
            this.settingsGrb.Controls.Add(this.nbSuccessLbl);
            this.settingsGrb.Controls.Add(this.nbTransitionLbl);
            this.settingsGrb.Controls.Add(this.minScoreNum);
            this.settingsGrb.Controls.Add(this.minScoreLbl);
            this.settingsGrb.Controls.Add(this.nbSuccessNum);
            this.settingsGrb.Controls.Add(this.nbTransitionNum);
            this.settingsGrb.Location = new System.Drawing.Point(60, 31);
            this.settingsGrb.Name = "settingsGrb";
            this.settingsGrb.Size = new System.Drawing.Size(454, 206);
            this.settingsGrb.TabIndex = 6;
            this.settingsGrb.TabStop = false;
            // 
            // lvl5Txt
            // 
            this.lvl5Txt.Location = new System.Drawing.Point(334, 50);
            this.lvl5Txt.Name = "lvl5Txt";
            this.lvl5Txt.Size = new System.Drawing.Size(76, 20);
            this.lvl5Txt.TabIndex = 16;
            // 
            // lvl5Lbl
            // 
            this.lvl5Lbl.AutoSize = true;
            this.lvl5Lbl.Location = new System.Drawing.Point(334, 21);
            this.lvl5Lbl.Name = "lvl5Lbl";
            this.lvl5Lbl.Size = new System.Drawing.Size(93, 26);
            this.lvl5Lbl.TabIndex = 15;
            this.lvl5Lbl.Text = "Niveau 5\r\nopérations ABxCD";
            // 
            // lvl4Txt
            // 
            this.lvl4Txt.Location = new System.Drawing.Point(252, 50);
            this.lvl4Txt.Name = "lvl4Txt";
            this.lvl4Txt.Size = new System.Drawing.Size(76, 20);
            this.lvl4Txt.TabIndex = 14;
            // 
            // lvl4Lbl
            // 
            this.lvl4Lbl.AutoSize = true;
            this.lvl4Lbl.Location = new System.Drawing.Point(252, 21);
            this.lvl4Lbl.Name = "lvl4Lbl";
            this.lvl4Lbl.Size = new System.Drawing.Size(85, 26);
            this.lvl4Lbl.TabIndex = 13;
            this.lvl4Lbl.Text = "Niveau 4\r\nopérations ABxC";
            // 
            // lvl3Txt
            // 
            this.lvl3Txt.Location = new System.Drawing.Point(170, 50);
            this.lvl3Txt.Name = "lvl3Txt";
            this.lvl3Txt.Size = new System.Drawing.Size(76, 20);
            this.lvl3Txt.TabIndex = 12;
            // 
            // lvl3Lbl
            // 
            this.lvl3Lbl.AutoSize = true;
            this.lvl3Lbl.Location = new System.Drawing.Point(170, 21);
            this.lvl3Lbl.Name = "lvl3Lbl";
            this.lvl3Lbl.Size = new System.Drawing.Size(77, 26);
            this.lvl3Lbl.TabIndex = 11;
            this.lvl3Lbl.Text = "Niveau 3\r\ntables de 7 à 9";
            // 
            // lvl2Txt
            // 
            this.lvl2Txt.Location = new System.Drawing.Point(88, 50);
            this.lvl2Txt.Name = "lvl2Txt";
            this.lvl2Txt.Size = new System.Drawing.Size(76, 20);
            this.lvl2Txt.TabIndex = 10;
            // 
            // lvl2Lbl
            // 
            this.lvl2Lbl.AutoSize = true;
            this.lvl2Lbl.Location = new System.Drawing.Point(88, 21);
            this.lvl2Lbl.Name = "lvl2Lbl";
            this.lvl2Lbl.Size = new System.Drawing.Size(77, 26);
            this.lvl2Lbl.TabIndex = 9;
            this.lvl2Lbl.Text = "Niveau 2\r\ntables de 3 à 6";
            // 
            // lvl1Txt
            // 
            this.lvl1Txt.Location = new System.Drawing.Point(6, 50);
            this.lvl1Txt.Name = "lvl1Txt";
            this.lvl1Txt.Size = new System.Drawing.Size(76, 20);
            this.lvl1Txt.TabIndex = 8;
            // 
            // lvl1Lbl
            // 
            this.lvl1Lbl.AutoSize = true;
            this.lvl1Lbl.Location = new System.Drawing.Point(6, 21);
            this.lvl1Lbl.Name = "lvl1Lbl";
            this.lvl1Lbl.Size = new System.Drawing.Size(77, 26);
            this.lvl1Lbl.TabIndex = 7;
            this.lvl1Lbl.Text = "Niveau 1\r\ntables de 1 à 3";
            // 
            // saveSettingsBtn
            // 
            this.saveSettingsBtn.Location = new System.Drawing.Point(373, 177);
            this.saveSettingsBtn.Name = "saveSettingsBtn";
            this.saveSettingsBtn.Size = new System.Drawing.Size(75, 23);
            this.saveSettingsBtn.TabIndex = 6;
            this.saveSettingsBtn.Text = "Sauvegarder";
            this.saveSettingsBtn.UseVisualStyleBackColor = true;
            this.saveSettingsBtn.Click += new System.EventHandler(this.saveSettingsBtn_Click);
            // 
            // nbSuccessLbl
            // 
            this.nbSuccessLbl.AutoSize = true;
            this.nbSuccessLbl.Location = new System.Drawing.Point(56, 152);
            this.nbSuccessLbl.Name = "nbSuccessLbl";
            this.nbSuccessLbl.Size = new System.Drawing.Size(267, 13);
            this.nbSuccessLbl.TabIndex = 5;
            this.nbSuccessLbl.Text = "Nombre de succès avant le passage au niveau suivant";
            // 
            // nbTransitionLbl
            // 
            this.nbTransitionLbl.AutoSize = true;
            this.nbTransitionLbl.Location = new System.Drawing.Point(195, 100);
            this.nbTransitionLbl.Name = "nbTransitionLbl";
            this.nbTransitionLbl.Size = new System.Drawing.Size(128, 13);
            this.nbTransitionLbl.TabIndex = 3;
            this.nbTransitionLbl.Text = "Nombre d\'états transitions";
            // 
            // minScoreNum
            // 
            this.minScoreNum.Location = new System.Drawing.Point(329, 124);
            this.minScoreNum.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.minScoreNum.Name = "minScoreNum";
            this.minScoreNum.Size = new System.Drawing.Size(120, 20);
            this.minScoreNum.TabIndex = 1;
            // 
            // minScoreLbl
            // 
            this.minScoreLbl.AutoSize = true;
            this.minScoreLbl.Location = new System.Drawing.Point(123, 126);
            this.minScoreLbl.Name = "minScoreLbl";
            this.minScoreLbl.Size = new System.Drawing.Size(200, 13);
            this.minScoreLbl.TabIndex = 4;
            this.minScoreLbl.Text = "Score minimal pour considérer un succès";
            // 
            // nbSuccessNum
            // 
            this.nbSuccessNum.Location = new System.Drawing.Point(329, 150);
            this.nbSuccessNum.Name = "nbSuccessNum";
            this.nbSuccessNum.Size = new System.Drawing.Size(120, 20);
            this.nbSuccessNum.TabIndex = 2;
            // 
            // nbTransitionNum
            // 
            this.nbTransitionNum.Location = new System.Drawing.Point(329, 98);
            this.nbTransitionNum.Name = "nbTransitionNum";
            this.nbTransitionNum.Size = new System.Drawing.Size(120, 20);
            this.nbTransitionNum.TabIndex = 0;
            // 
            // adminSpl
            // 
            this.adminSpl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.adminSpl.Location = new System.Drawing.Point(0, 0);
            this.adminSpl.Name = "adminSpl";
            this.adminSpl.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // adminSpl.Panel1
            // 
            this.adminSpl.Panel1.Controls.Add(this.changeSettingsGrb);
            // 
            // adminSpl.Panel2
            // 
            this.adminSpl.Panel2.Controls.Add(this.manageProfileGrb);
            this.adminSpl.Size = new System.Drawing.Size(579, 517);
            this.adminSpl.SplitterDistance = 258;
            this.adminSpl.TabIndex = 8;
            // 
            // addPlayTestBtn
            // 
            this.addPlayTestBtn.Enabled = false;
            this.addPlayTestBtn.Location = new System.Drawing.Point(219, 26);
            this.addPlayTestBtn.Name = "addPlayTestBtn";
            this.addPlayTestBtn.Size = new System.Drawing.Size(120, 23);
            this.addPlayTestBtn.TabIndex = 9;
            this.addPlayTestBtn.Text = "Ajouter un jeu d\'essai";
            this.addPlayTestBtn.UseVisualStyleBackColor = true;
            this.addPlayTestBtn.Click += new System.EventHandler(this.addPlayTestBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(345, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "(Attention, l\'utilisateur passera au niveau 4)";
            // 
            // AdminPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.adminSpl);
            this.Name = "AdminPanel";
            this.Size = new System.Drawing.Size(579, 517);
            this.manageProfileGrb.ResumeLayout(false);
            this.manageProfileGrb.PerformLayout();
            this.profileOptionsGrb.ResumeLayout(false);
            this.profileOptionsGrb.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transitionNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transitionTrb)).EndInit();
            this.changeSettingsGrb.ResumeLayout(false);
            this.settingsGrb.ResumeLayout(false);
            this.settingsGrb.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minScoreNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbSuccessNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbTransitionNum)).EndInit();
            this.adminSpl.Panel1.ResumeLayout(false);
            this.adminSpl.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.adminSpl)).EndInit();
            this.adminSpl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.ComboBox loginsCbb;
        private System.Windows.Forms.Label loginLbl;
        private System.Windows.Forms.GroupBox manageProfileGrb;
        private System.Windows.Forms.GroupBox changeSettingsGrb;
        private System.Windows.Forms.Label nbSuccessLbl;
        private System.Windows.Forms.Label minScoreLbl;
        private System.Windows.Forms.Label nbTransitionLbl;
        private System.Windows.Forms.SplitContainer adminSpl;
        private System.Windows.Forms.GroupBox settingsGrb;
        private System.Windows.Forms.Button saveSettingsBtn;
        private System.Windows.Forms.GroupBox profileOptionsGrb;
        public System.Windows.Forms.ComboBox levelCbb;
        private System.Windows.Forms.TextBox firstnameTxt;
        private System.Windows.Forms.TextBox nameTxt;
        private System.Windows.Forms.TextBox usernameTxt;
        private System.Windows.Forms.CheckBox evolvingChb;
        private System.Windows.Forms.TrackBar transitionTrb;
        private System.Windows.Forms.NumericUpDown transitionNum;
        private System.Windows.Forms.Label levelLbl;
        private System.Windows.Forms.Label firstnameLbl;
        private System.Windows.Forms.Label nameLbl;
        private System.Windows.Forms.Label usernameLbl;
        private System.Windows.Forms.Label transitionLbl;
        private System.Windows.Forms.Button saveProfileBtn;
        private System.Windows.Forms.TextBox lvl5Txt;
        private System.Windows.Forms.Label lvl5Lbl;
        private System.Windows.Forms.TextBox lvl4Txt;
        private System.Windows.Forms.Label lvl4Lbl;
        private System.Windows.Forms.TextBox lvl3Txt;
        private System.Windows.Forms.Label lvl3Lbl;
        private System.Windows.Forms.TextBox lvl2Txt;
        private System.Windows.Forms.Label lvl2Lbl;
        private System.Windows.Forms.TextBox lvl1Txt;
        private System.Windows.Forms.Label lvl1Lbl;
        private System.Windows.Forms.NumericUpDown nbTransitionNum;
        private System.Windows.Forms.NumericUpDown minScoreNum;
        private System.Windows.Forms.NumericUpDown nbSuccessNum;
        private System.Windows.Forms.Button resultsBtn;
        private System.Windows.Forms.Button addPlayTestBtn;
        private System.Windows.Forms.Label label1;
    }
}
