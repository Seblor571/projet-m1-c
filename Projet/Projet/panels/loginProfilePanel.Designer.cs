﻿namespace Projet.Panels {
    partial class LoginProfilePanel {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.loginGrb = new System.Windows.Forms.GroupBox();
            this.loginsCbb = new System.Windows.Forms.ComboBox();
            this.passTxt = new System.Windows.Forms.TextBox();
            this.passLbl = new System.Windows.Forms.Label();
            this.loginLbl = new System.Windows.Forms.Label();
            this.loginBtn = new System.Windows.Forms.Button();
            this.loginGrb.SuspendLayout();
            this.SuspendLayout();
            // 
            // loginGrb
            // 
            this.loginGrb.Controls.Add(this.loginsCbb);
            this.loginGrb.Controls.Add(this.passTxt);
            this.loginGrb.Controls.Add(this.passLbl);
            this.loginGrb.Controls.Add(this.loginLbl);
            this.loginGrb.Controls.Add(this.loginBtn);
            this.loginGrb.Location = new System.Drawing.Point(30, 70);
            this.loginGrb.Name = "loginGrb";
            this.loginGrb.Size = new System.Drawing.Size(283, 149);
            this.loginGrb.TabIndex = 1;
            this.loginGrb.TabStop = false;
            this.loginGrb.Text = "Connection";
            // 
            // loginsCbb
            // 
            this.loginsCbb.FormattingEnabled = true;
            this.loginsCbb.Location = new System.Drawing.Point(108, 31);
            this.loginsCbb.Name = "loginsCbb";
            this.loginsCbb.Size = new System.Drawing.Size(169, 21);
            this.loginsCbb.TabIndex = 4;
            // 
            // passTxt
            // 
            this.passTxt.Location = new System.Drawing.Point(108, 55);
            this.passTxt.Name = "passTxt";
            this.passTxt.Size = new System.Drawing.Size(169, 20);
            this.passTxt.TabIndex = 5;
            this.passTxt.UseSystemPasswordChar = true;
            // 
            // passLbl
            // 
            this.passLbl.AutoSize = true;
            this.passLbl.Location = new System.Drawing.Point(31, 58);
            this.passLbl.Name = "passLbl";
            this.passLbl.Size = new System.Drawing.Size(71, 13);
            this.passLbl.TabIndex = 2;
            this.passLbl.Text = "Mot de passe";
            // 
            // loginLbl
            // 
            this.loginLbl.AutoSize = true;
            this.loginLbl.Location = new System.Drawing.Point(49, 34);
            this.loginLbl.Name = "loginLbl";
            this.loginLbl.Size = new System.Drawing.Size(53, 13);
            this.loginLbl.TabIndex = 1;
            this.loginLbl.Text = "Identifiant";
            // 
            // loginBtn
            // 
            this.loginBtn.Location = new System.Drawing.Point(184, 120);
            this.loginBtn.Name = "loginBtn";
            this.loginBtn.Size = new System.Drawing.Size(93, 23);
            this.loginBtn.TabIndex = 6;
            this.loginBtn.Text = "Connection";
            this.loginBtn.UseVisualStyleBackColor = true;
            // 
            // loginProfilePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.loginGrb);
            this.Name = "loginProfilePanel";
            this.Size = new System.Drawing.Size(331, 304);
            this.Resize += new System.EventHandler(this.loginProfilePanel_Resize);
            this.loginGrb.ResumeLayout(false);
            this.loginGrb.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox loginGrb;
        private System.Windows.Forms.Label passLbl;
        private System.Windows.Forms.Label loginLbl;
        public System.Windows.Forms.ComboBox loginsCbb;
        public System.Windows.Forms.TextBox passTxt;
        public System.Windows.Forms.Button loginBtn;
    }
}
