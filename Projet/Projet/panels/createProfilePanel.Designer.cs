﻿namespace Projet.Panels {
    partial class CreateProfilePanel {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.createProfileGrb = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.passVerifyTxt = new System.Windows.Forms.TextBox();
            this.passTxt = new System.Windows.Forms.TextBox();
            this.createProfileBtn = new System.Windows.Forms.Button();
            this.levelLbl = new System.Windows.Forms.Label();
            this.firstnameLbl = new System.Windows.Forms.Label();
            this.nameLbl = new System.Windows.Forms.Label();
            this.usernameLbl = new System.Windows.Forms.Label();
            this.transitionLbl = new System.Windows.Forms.Label();
            this.transitionNum = new System.Windows.Forms.NumericUpDown();
            this.evolvingChb = new System.Windows.Forms.CheckBox();
            this.transitionTrb = new System.Windows.Forms.TrackBar();
            this.levelCbb = new System.Windows.Forms.ComboBox();
            this.firstnameTxt = new System.Windows.Forms.TextBox();
            this.nameTxt = new System.Windows.Forms.TextBox();
            this.usernameTxt = new System.Windows.Forms.TextBox();
            this.createProfileGrb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transitionNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transitionTrb)).BeginInit();
            this.SuspendLayout();
            // 
            // createProfileGrb
            // 
            this.createProfileGrb.Controls.Add(this.label2);
            this.createProfileGrb.Controls.Add(this.label1);
            this.createProfileGrb.Controls.Add(this.passVerifyTxt);
            this.createProfileGrb.Controls.Add(this.passTxt);
            this.createProfileGrb.Controls.Add(this.createProfileBtn);
            this.createProfileGrb.Controls.Add(this.levelLbl);
            this.createProfileGrb.Controls.Add(this.firstnameLbl);
            this.createProfileGrb.Controls.Add(this.nameLbl);
            this.createProfileGrb.Controls.Add(this.usernameLbl);
            this.createProfileGrb.Controls.Add(this.transitionLbl);
            this.createProfileGrb.Controls.Add(this.transitionNum);
            this.createProfileGrb.Controls.Add(this.evolvingChb);
            this.createProfileGrb.Controls.Add(this.transitionTrb);
            this.createProfileGrb.Controls.Add(this.levelCbb);
            this.createProfileGrb.Controls.Add(this.firstnameTxt);
            this.createProfileGrb.Controls.Add(this.nameTxt);
            this.createProfileGrb.Controls.Add(this.usernameTxt);
            this.createProfileGrb.Location = new System.Drawing.Point(18, 117);
            this.createProfileGrb.Name = "createProfileGrb";
            this.createProfileGrb.Size = new System.Drawing.Size(562, 168);
            this.createProfileGrb.TabIndex = 9;
            this.createProfileGrb.TabStop = false;
            this.createProfileGrb.Text = "Création d\'un profil";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Confirmer le mot de passe";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Mot de passe";
            // 
            // passVerifyTxt
            // 
            this.passVerifyTxt.Location = new System.Drawing.Point(149, 135);
            this.passVerifyTxt.MaxLength = 64;
            this.passVerifyTxt.Name = "passVerifyTxt";
            this.passVerifyTxt.Size = new System.Drawing.Size(100, 20);
            this.passVerifyTxt.TabIndex = 4;
            this.passVerifyTxt.UseSystemPasswordChar = true;
            this.passVerifyTxt.TextChanged += new System.EventHandler(this.elementChanged);
            // 
            // passTxt
            // 
            this.passTxt.Location = new System.Drawing.Point(149, 109);
            this.passTxt.MaxLength = 64;
            this.passTxt.Name = "passTxt";
            this.passTxt.Size = new System.Drawing.Size(100, 20);
            this.passTxt.TabIndex = 3;
            this.passTxt.UseSystemPasswordChar = true;
            this.passTxt.TextChanged += new System.EventHandler(this.elementChanged);
            // 
            // createProfileBtn
            // 
            this.createProfileBtn.Enabled = false;
            this.createProfileBtn.Location = new System.Drawing.Point(450, 139);
            this.createProfileBtn.Name = "createProfileBtn";
            this.createProfileBtn.Size = new System.Drawing.Size(106, 23);
            this.createProfileBtn.TabIndex = 9;
            this.createProfileBtn.Text = "Créer le profil";
            this.createProfileBtn.UseVisualStyleBackColor = true;
            // 
            // levelLbl
            // 
            this.levelLbl.AutoSize = true;
            this.levelLbl.Location = new System.Drawing.Point(306, 34);
            this.levelLbl.Name = "levelLbl";
            this.levelLbl.Size = new System.Drawing.Size(41, 13);
            this.levelLbl.TabIndex = 11;
            this.levelLbl.Text = "Niveau";
            // 
            // firstnameLbl
            // 
            this.firstnameLbl.AutoSize = true;
            this.firstnameLbl.Location = new System.Drawing.Point(100, 86);
            this.firstnameLbl.Name = "firstnameLbl";
            this.firstnameLbl.Size = new System.Drawing.Size(43, 13);
            this.firstnameLbl.TabIndex = 10;
            this.firstnameLbl.Text = "Prénom";
            // 
            // nameLbl
            // 
            this.nameLbl.AutoSize = true;
            this.nameLbl.Location = new System.Drawing.Point(114, 60);
            this.nameLbl.Name = "nameLbl";
            this.nameLbl.Size = new System.Drawing.Size(29, 13);
            this.nameLbl.TabIndex = 9;
            this.nameLbl.Text = "Nom";
            // 
            // usernameLbl
            // 
            this.usernameLbl.AutoSize = true;
            this.usernameLbl.Location = new System.Drawing.Point(59, 34);
            this.usernameLbl.Name = "usernameLbl";
            this.usernameLbl.Size = new System.Drawing.Size(84, 13);
            this.usernameLbl.TabIndex = 8;
            this.usernameLbl.Text = "Nom d\'utilisateur";
            // 
            // transitionLbl
            // 
            this.transitionLbl.AutoSize = true;
            this.transitionLbl.Location = new System.Drawing.Point(296, 62);
            this.transitionLbl.Name = "transitionLbl";
            this.transitionLbl.Size = new System.Drawing.Size(53, 13);
            this.transitionLbl.TabIndex = 7;
            this.transitionLbl.Text = "Transition";
            // 
            // transitionNum
            // 
            this.transitionNum.Location = new System.Drawing.Point(496, 60);
            this.transitionNum.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.transitionNum.Name = "transitionNum";
            this.transitionNum.Size = new System.Drawing.Size(50, 20);
            this.transitionNum.TabIndex = 7;
            this.transitionNum.ValueChanged += new System.EventHandler(this.transitionNum_ValueChanged);
            // 
            // evolvingChb
            // 
            this.evolvingChb.AutoSize = true;
            this.evolvingChb.Checked = true;
            this.evolvingChb.CheckState = System.Windows.Forms.CheckState.Checked;
            this.evolvingChb.Location = new System.Drawing.Point(355, 111);
            this.evolvingChb.Name = "evolvingChb";
            this.evolvingChb.Size = new System.Drawing.Size(59, 17);
            this.evolvingChb.TabIndex = 8;
            this.evolvingChb.Text = "Evolue";
            this.evolvingChb.UseVisualStyleBackColor = true;
            // 
            // transitionTrb
            // 
            this.transitionTrb.LargeChange = 1;
            this.transitionTrb.Location = new System.Drawing.Point(355, 60);
            this.transitionTrb.Maximum = 3;
            this.transitionTrb.Name = "transitionTrb";
            this.transitionTrb.Size = new System.Drawing.Size(135, 45);
            this.transitionTrb.TabIndex = 6;
            this.transitionTrb.Scroll += new System.EventHandler(this.transitionTrb_Scroll);
            // 
            // levelCbb
            // 
            this.levelCbb.FormattingEnabled = true;
            this.levelCbb.Location = new System.Drawing.Point(355, 31);
            this.levelCbb.Name = "levelCbb";
            this.levelCbb.Size = new System.Drawing.Size(121, 21);
            this.levelCbb.TabIndex = 5;
            this.levelCbb.SelectedIndexChanged += new System.EventHandler(this.levelCbb_SelectedIndexChanged);
            // 
            // firstnameTxt
            // 
            this.firstnameTxt.Location = new System.Drawing.Point(149, 83);
            this.firstnameTxt.Name = "firstnameTxt";
            this.firstnameTxt.Size = new System.Drawing.Size(100, 20);
            this.firstnameTxt.TabIndex = 2;
            this.firstnameTxt.TextChanged += new System.EventHandler(this.elementChanged);
            // 
            // nameTxt
            // 
            this.nameTxt.Location = new System.Drawing.Point(149, 57);
            this.nameTxt.Name = "nameTxt";
            this.nameTxt.Size = new System.Drawing.Size(100, 20);
            this.nameTxt.TabIndex = 1;
            this.nameTxt.TextChanged += new System.EventHandler(this.elementChanged);
            // 
            // usernameTxt
            // 
            this.usernameTxt.Location = new System.Drawing.Point(149, 31);
            this.usernameTxt.Name = "usernameTxt";
            this.usernameTxt.Size = new System.Drawing.Size(100, 20);
            this.usernameTxt.TabIndex = 0;
            this.usernameTxt.TextChanged += new System.EventHandler(this.elementChanged);
            // 
            // CreateProfilePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.createProfileGrb);
            this.Name = "CreateProfilePanel";
            this.Size = new System.Drawing.Size(601, 447);
            this.Resize += new System.EventHandler(this.createProfilePanel_Resize);
            this.createProfileGrb.ResumeLayout(false);
            this.createProfileGrb.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transitionNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transitionTrb)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox createProfileGrb;
        private System.Windows.Forms.Label levelLbl;
        private System.Windows.Forms.Label firstnameLbl;
        private System.Windows.Forms.Label nameLbl;
        private System.Windows.Forms.Label usernameLbl;
        private System.Windows.Forms.Label transitionLbl;
        private System.Windows.Forms.NumericUpDown transitionNum;
        private System.Windows.Forms.CheckBox evolvingChb;
        private System.Windows.Forms.TrackBar transitionTrb;
        public System.Windows.Forms.ComboBox levelCbb;
        private System.Windows.Forms.TextBox firstnameTxt;
        private System.Windows.Forms.TextBox nameTxt;
        private System.Windows.Forms.TextBox usernameTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox passVerifyTxt;
        private System.Windows.Forms.TextBox passTxt;
        public System.Windows.Forms.Button createProfileBtn;
    }
}
