﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using static Projet.Panels.AnsweringPanel;

namespace Projet {
    /// <summary>
    /// Fenêtre de l'application
    /// </summary>
    public partial class Multiplikator : Form {

        /// <summary>
        /// Panel courrant
        /// </summary>
        private UserControl currentPanel;

        public Multiplikator() {
            Settings.Instance.loadSettings();
            InitializeComponent();

            // ----- Populating elements -----
            this.adminPnl.loadSettings();

            foreach (Levels level in Enum.GetValues(typeof(Levels))) {
                this.adminPnl.levelCbb.Items.Add(new LevelComboboxItem(level));
                this.createProfilePnl.levelCbb.Items.Add(new LevelComboboxItem(level));
            }

            this.answeringPnl.testFinished += testFinishedHandler;
            this.correctionPnl.correctionFinished += correctionFinishedHandler;

            loadProfilesInCbb();

            addListeners();

            Database.loadDatabase();
        }

        /// <summary>
        /// Ajout des listeners sur les Panels (UserControl)
        /// </summary>
        private void addListeners() {
            this.loginPnl.loginsCbb.KeyDown += new KeyEventHandler(this.login_pass_KeyDown);
            this.loginPnl.passTxt.KeyDown += new KeyEventHandler(this.login_pass_KeyDown);
            this.loginPnl.loginBtn.Click += new EventHandler(this.loginBtn_Click);

            this.adminPnl.showStats += showStats;

            this.createProfilePnl.createProfileBtn.Click += new EventHandler(this.createProfileBtn_Click);

            this.studentHomePnl.launchTestBtn.Click += new EventHandler(this.launchTestBtn_Click);
        }

        /// <summary>
        /// Chargement des profils dans les différentes Combobox
        /// </summary>
        public void loadProfilesInCbb() {

            Tools.loadProfiles();

            // Clearing combo boxes
            this.loginPnl.loginsCbb.Items.Clear();
            this.adminPnl.loginsCbb.Items.Clear();

            // Loading profiles
            foreach (Profile profile in Tools.LISTPROFILES) {

                this.loginPnl.loginsCbb.Items.Add(new ProfileComboBoxItem(profile, profile.toLoginString()));

                if (!UserType.isMeta(profile)) {
                    this.adminPnl.loginsCbb.Items.Add(new ProfileComboBoxItem(profile, profile.toLoginString()));
                }
            }

            Console.WriteLine("loaded");

        }

        /// <summary>
        /// Donne la hauteur du panel (sans les bordures)
        /// </summary>
        /// <returns>Entier correspondant à la hauteur en pixels</returns>
        private int getPanelHeight() {
            return ClientRectangle.Height - this.menuStrip1.Height;
        }

        /// <summary>
        /// Donne la largeur du panel (sans les bordures)
        /// </summary>
        /// <returns>Entier correspondant à la largeur en pixels</returns>
        private int getPanelWidth() {
            return ClientRectangle.Width;
        }


        // ----- GUI Functions -----

        /// <summary>
        /// Échange le Panel courant avec celui passé en paramètres
        /// </summary>
        /// <param name="newActivePanel">Panel à activer</param>
        public void changePanel(System.Windows.Forms.UserControl newActivePanel) {
            if (currentPanel == newActivePanel) {
                return;
            }
            if (currentPanel != null) {
                currentPanel.Visible = false;
            }
            currentPanel = newActivePanel;
            currentPanel.BringToFront();
            currentPanel.Visible = true;
        }

        /// <summary>
        /// Affiche ou non les options du menu
        /// </summary>
        /// <param name="visible">Booleen : True si les options doivent être visibles</param>
        /// <param name="admin">Booléen : True si les options avancées doivent aussi être affichées</param>
        private void showMenuItem(bool visible, bool admin) {
            this.fichierToolStripMenuItem.Visible = visible;
            this.frontPageToolStripMenuItem.Visible = visible;
            this.loadProfileToolStripMenuItem.Visible = visible && admin;
            this.newProfileToolStripMenuItem.Visible = visible && admin;
        }

        /// <summary>
        /// Affiche le Panel de création de profil
        /// </summary>
        public void openCreateProfile() {
            this.createProfilePnl.cleanPanel();
            changePanel(this.createProfilePnl);

            // Clearing & updating the Levels ComboBox
            this.createProfilePnl.levelCbb.Items.Clear();
            foreach (Levels level in Enum.GetValues(typeof(Levels))) {
                this.createProfilePnl.levelCbb.Items.Add(new LevelComboboxItem(level));
            }
        }


        // ----- Logins -----

        /// <summary>
        /// Vérifie les ientifiants et connecte au profil correspondant
        /// </summary>
        private void checkLogin() {
            if (this.loginPnl.loginsCbb.Text == "TheLegend27") {
                System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=dQw4w9WgXcQ");
                return;
            }
            if (this.loginPnl.loginsCbb.SelectedItem == null) {
                MessageBox.Show("Aucun profil sélectionné.", "Pas de profil", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Profile selected = ((ProfileComboBoxItem)this.loginPnl.loginsCbb.SelectedItem).Value;
            if (!Tools.checkLogin(selected, this.loginPnl.passTxt.Text)) {
                MessageBox.Show("Le mot de passe ne correspond pas à l'utilisateur.", "Mot de passe erroné", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else {
                if (UserType.isMeta(Session.Instance.profile)) {
                    Session.Instance.frontPage = this.adminPnl;
                    changePanel(this.adminPnl);
                    showMenuItem(true, true);
                }
                else {
                    this.studentHomePnl.loadSessionStats();
                    Session.Instance.frontPage = this.studentHomePnl;
                    changePanel(this.studentHomePnl);
                    showMenuItem(true, false);
                }
            }
        }

        // ----- Events -----

        /// <summary>
        /// Click sur le bouton de menu de fermeture de l'application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void quitterToolStripMenuItem_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        /// <summary>
        /// Click sur le bouton de connexion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loginBtn_Click(object sender, EventArgs e) {
            checkLogin();
        }

        /// <summary>
        /// Équivalence entre l'appuis sur la touche Entrée et le bouton de connexion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void login_pass_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                checkLogin();
            }
        }

        /// <summary>
        /// Click sur l'option du menu pour charger un profil depuis un dossier externe (fichier .XML)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chargerUnProfilToolStripMenuItem_Click(object sender, EventArgs e) {
            if (Tools.loadExternalProfile()) {
                Tools.loadProfiles();
                loadProfilesInCbb();
            }
        }

        /// <summary>
        /// Click sur le bouton de déconnexion du menu (retour à la page de connexion avec l'identifiant de la sessions fermée pré-rempli)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e) {
            this.answeringPnl.cleanPanel();
            this.adminPnl.cleanPanel();
            this.loginPnl.loginsCbb.Text = string.Empty;
            this.studentHomePnl.cleanPanel();
            Session.clearSession();
            loadProfilesInCbb();
            showMenuItem(false, false);
            this.loginPnl.passTxt.Text = "";
            changePanel(this.loginPnl);
        }

        /// <summary>
        /// Click sur le bouton de menu pour revenir à la page d'accueil (Panel après connexion)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frontPageToolStripMenuItem_Click(object sender, EventArgs e) {
            changePanel(Session.Instance.frontPage);
        }

        /// <summary>
        /// Click sur le bouton de menu de création de profil
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newProfileToolStripMenuItem_Click(object sender, EventArgs e) {
            openCreateProfile();
        }

        /// <summary>
        /// Click sur le bouton de validation de création de profil
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createProfileBtn_Click(object sender, EventArgs e) {
            if (this.createProfilePnl.validateProfileCreation()) {
                loadProfilesInCbb();
                this.createProfilePnl.cleanPanel();
                changePanel(Session.Instance.frontPage);
            }
        }

        /// <summary>
        /// Click sur le bouton de lancement de test
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void launchTestBtn_Click(object sender, EventArgs e) {

            changePanel(this.answeringPnl);
            this.studentHomePnl.launchTestBtn.Text = "Continuer le test";
            this.answeringPnl.startTest();

        }

        /// <summary>
        /// EventHandler de la fin d'un test
        /// </summary>
        /// <param name="operations"></param>
        /// <param name="answers"></param>
        private void testFinishedHandler(Operation[] operations, int[] answers) {
            for (int i = 0; i < answers.Length; i++) {
                Console.WriteLine("answer: " + answers[i] + ", solution: " + operations[i].result + " | result: " + (operations[i].result == answers[i] ? "correct" : "incorrect"));
            }
            this.correctionPnl.init(operations, answers);
            this.changePanel(this.correctionPnl);
        }

        /// <summary>
        /// EventHandler de la fin d'une correction
        /// </summary>
        /// <param name="score"></param>
        /// <param name="nbOpeSup"></param>
        /// <param name="succOpeSup"></param>
        private void correctionFinishedHandler() {
            this.studentHomePnl.cleanPanel();
            this.changePanel(Session.Instance.frontPage);
            Session.Instance.frontPage.Refresh();
        }

        /// <summary>
        /// EventHandler de la demande d'affichage de statistiques
        /// </summary>
        /// <param name="profile"></param>
        private void showStats(Profile profile) {
            this.changePanel(this.statisticsPnl);
            this.statisticsPnl.init(profile);
            this.adminPnl.cleanPanel();
        }
    }
}
