﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet {
    /// <summary>
    /// Types d'utilisateurs
    /// </summary>
    public enum UserTypes { ADMINISTRATOR, SUPERVISOR, STUDENT };

    public static class UserType {

        /// <summary>
        /// Returns name for Administrator & Supervisor UserTypes
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string getName(this UserTypes type) {
            switch (type) {
                case UserTypes.ADMINISTRATOR:
                    return "Administrateur";
                case UserTypes.SUPERVISOR:
                    return "Superviseur";
            }
            return "";
        }

        public static bool isMeta(Profile profile) {
            return (profile._username == UserTypes.ADMINISTRATOR.getName() || profile._username == UserTypes.SUPERVISOR.getName());
        }
    }
}
