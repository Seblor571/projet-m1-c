﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Projet.Panels {
    /// <summary>
    /// Panneau de correction des opérations
    /// </summary>
    public partial class CorrectionPanel : UserControl, PanelInterface {

        /// <summary>
        /// Liste des panels d'opération
        /// </summary>
        private List<Panel> opePanels;

        /// <summary>
        /// Liste des labels d'opération
        /// </summary>
        private List<Label> opeLabels;

        /// <summary>
        /// Liste des opérations
        /// </summary>
        private Operation[] operations;

        /// <summary>
        /// Indice de l'opération en coure de correction
        /// </summary>
        private int currentOpe;

        /// <summary>
        /// Évènement de la fin de la correction
        /// </summary>
        public event correctionFinishedHandler correctionFinished;

        /// <summary>
        /// Délégué de la fin de la correction
        /// </summary>
        public delegate void correctionFinishedHandler();

        /// <summary>
        /// Niveau du test
        /// </summary>
        private Levels baseLevel;

        public CorrectionPanel() {

            InitializeComponent();

            this.opePanels = new List<Panel>(20);
            this.opeLabels = new List<Label>(20);
            for (int i = 0; i < 20; i++) {
                Panel panel = new Panel();
                this.OperationsPnl.Controls.Add(panel);
                panel.Dock = DockStyle.Top;
                //panel.Size = new System.Drawing.Size(168, 50);
                //panel.Location = new System.Drawing.Point(5, 5);
                panel.AutoSize = true;
                panel.AutoSizeMode = AutoSizeMode.GrowAndShrink;

                Label label = new Label();
                label.Font = new Font(FontFamily.GenericSansSerif, 13);
                label.AutoSize = true;
                label.Margin = new Padding(15);

                panel.Controls.Add(label);

                this.opePanels.Add(panel);
                this.opeLabels.Add(label);

            }
        }

        public void cleanPanel() {
            for (int i = 0; i < 20; i++) {
                this.opePanels[i].Visible = false;
                this.opeLabels[i].Text = "";
            }
        }

        /// <summary>
        /// Initialise toutes les corrections
        /// </summary>
        /// <param name="operations"></param>
        /// <param name="answers"></param>
        public void init(Operation[] operations, int[] answers) {
            int scoreResult = 0, nbOpeSup = 0, succOpeSup = 0;

            this.nextOpeBtn.Text = "Opération suivante";
            this.nextOpeBtn.Left = (this.ClientSize.Width - this.nextOpeBtn.Width) - 5;
            this.nextOpeBtn.Enabled = true;
            this.repeatBtn.Enabled = true;

            this.operations = operations;
            this.currentOpe = 0;

            this.baseLevel = operations[0].level;

            for (int i = 0; i < 20; i++) {

                // Checking for lower level
                if (this.baseLevel > operations[i].level) {
                    // If higher level

                    this.baseLevel = operations[i].level;

                    // Reset higher level operations stats
                    nbOpeSup = 1;
                    succOpeSup = 0;
                }
                else if (this.baseLevel != operations[i].level) {
                    // If upper level
                    nbOpeSup++;
                }

                this.opePanels[i].Visible = false;
                this.opeLabels[i].Text = operations[i].ToStringWithAnswer();
                if (operations[i].result == answers[i]) {
                    this.opeLabels[i].Text += " Correct !";
                    scoreResult++;

                    if (this.baseLevel != operations[i].level) {
                        succOpeSup++;
                    }

                }
                else {
                    this.opeLabels[i].Text += " Faux ! (Vous avez répondu " + answers[i] + ")";
                }
            }
            this.scoreLbl.Text = "Vous avez obtenu un score de " + scoreResult.ToString() + "/20";
            this.correctCurrentOperation();

            // Storing results in database
            Database.insertResult(Session.Instance.profile._username, this.baseLevel, scoreResult, nbOpeSup, succOpeSup);

            // Evolving profile
            if (Session.Instance.profile._evolving) {
                List<int> scores = Database.requestLastScores(Session.Instance.profile._username, Settings.Instance.nbSuccess, Session.Instance.profile._level);
                if (scores == null) {
                    // If not enough elements, can't evolve profile.
                    return;
                }

                foreach (int score in scores) {
                    if (score < Settings.Instance.minimumScore) {
                        // If score lower that minimum for success
                        return;
                    }
                }

                if (Session.Instance.profile.incrementLevel()) {
                    MessageBox.Show("Bravo ! tu as fini tous les niveaux de difficulté !");
                    Session.Instance.profile._evolving = false;
                    Session.Instance.profile.saveProfile();
                }
            }
        }

        // ----- Events -----

        /// <summary>
        /// Redimentionnement du panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CorrectionPanel_Resize(object sender, EventArgs e) {
            this.nextOpeBtn.Left = (this.ClientSize.Width - this.nextOpeBtn.Width) - 5;
            this.repeatBtn.Left = (this.ClientSize.Width - this.repeatBtn.Width) / 2;
        }
        
        /// <summary>
        /// Click sur le bouton de répétition de la réponse
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void repeatBtn_Click(object sender, EventArgs e) {
            this.operations[this.currentOpe].toSpeechCorrect();
        }

        /// <summary>
        /// Click sur le bouton de correction de l'opération suivante
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nextOpeBtn_Click(object sender, EventArgs e) {
            this.currentOpe++;
            this.correctCurrentOperation();
        }

        /// <summary>
        /// Correction de l'opération en cours de correction, et passage à l'opération suivante
        /// </summary>
        private void correctCurrentOperation() {
            if (currentOpe < 20) {
                this.operations[this.currentOpe].toSpeechCorrect();
                this.opePanels[this.currentOpe].Visible = true;
                if (currentOpe == 19) {
                    this.nextOpeBtn.Text = "Terminer la correction";
                    this.nextOpeBtn.Left = (this.ClientSize.Width - this.nextOpeBtn.Width) - 5;
                }
            }
            else {
                correctionFinished?.Invoke();
                this.nextOpeBtn.Enabled = false;
                this.repeatBtn.Enabled = false;
            }
        }
    }
}
