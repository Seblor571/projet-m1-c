﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Projet.Panels {
    /// <summary>
    /// Panneau de statistiques
    /// </summary>
    public partial class StatisticsPanel : UserControl, PanelInterface {
        public StatisticsPanel() {
            InitializeComponent();
        }

        /// <summary>
        /// (Re)création des séries du  graph (en cas de changement de nom)
        /// </summary>
        private void resetAxis() {

            dataGridView1.ReadOnly = true;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            dataGridView1.Columns.Clear();
            dataGridView1.Columns.Add("date", "Date");
            dataGridView1.Columns.Add("score", "Score");
            dataGridView1.Columns.Add("level", "Niveau");
            dataGridView1.Columns.Add("nbOpeSup", "Nombre d'opérations du niveau supérieur");
            dataGridView1.Columns.Add("succOpeSup", "Opérations du niveau supérieur réussies");


            // Clearing all series
            chart1.Series.Clear();

            // Creating one serie per level (line-type)
            foreach (Levels level in Enum.GetValues(typeof(Levels))) {
                chart1.Series.Add(level.toString());
                chart1.Series.Last().ChartType = SeriesChartType.Line;
                chart1.Series.Last().BorderWidth = 5;

                chart1.ChartAreas[chart1.Series.Last().ChartArea].AxisX.Minimum = 1;
                chart1.ChartAreas[chart1.Series.Last().ChartArea].AxisY.Minimum = 0;
                chart1.ChartAreas[chart1.Series.Last().ChartArea].AxisY.Maximum = 100;
                chart1.ChartAreas[chart1.Series.Last().ChartArea].CursorX.IsUserSelectionEnabled = true;
                //chart1.ChartAreas[chart1.Series.Last().ChartArea].AxisX.ScaleView.Zoomable = false;
                chart1.ChartAreas[chart1.Series.Last().ChartArea].CursorX.AutoScroll = true;

                chart1.Series.Last().Points.Clear();
            }
        }

        /// <summary>
        /// Initialise le graphique et le tableau de données
        /// </summary>
        /// <param name="profile"></param>
        public void init(Profile profile) {
            resetAxis();

            this.statLbl.Text = String.Format("Statistiques de {0} ({1} {2}).", profile._username, profile._firstname, profile._name.ToUpper());

            // On affiche le maximum de résultats
            List<TestResult> results =  Database.requestLastResults(profile._username);

            int testIndex = 1;

            foreach (TestResult result in results) {
                // Adding data into table
                dataGridView1.Rows.Add(new object[] { result.date, result.score, result.level.toString(), result.nbOpeSup, result.succOpeSup });


                chart1.Series[result.level.getIntegerId()].Points.InsertXY(chart1.Series[result.level.getIntegerId()].Points.Count, testIndex, ((result.score - result.succOpeSup) * 100) / (20 - result.nbOpeSup));

                if (result.nbOpeSup > 0) {
                    chart1.Series[result.level.getIntegerId() + 1].Points.InsertXY(chart1.Series[result.level.getIntegerId() + 1].Points.Count, testIndex, (result.succOpeSup * 100) / (result.nbOpeSup));
                }

                testIndex++;
            }

            testIndex--;
            chart1.ChartAreas[chart1.Series.Last().ChartArea].AxisX.Maximum = testIndex;
            chart1.ChartAreas[chart1.Series.Last().ChartArea].AxisX.ScaleView.Zoom(testIndex - 13, testIndex - 1);

            // Formating table
            dataGridView1.AutoResizeRowHeadersWidth(DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders);
            // Focusing last row
            dataGridView1.CurrentCell = dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0];
            dataGridView1.CurrentRow.Selected = false;
        }

        public void cleanPanel() {
            chart1.Series[0].Points.Clear();
            dataGridView1.Rows.Clear();
            this.statLbl.Text = "Statistiques";
        }

        /// <summary>
        /// EventHandler d'ajour de ligne (utilisé pour ajouter le numéro de ligne)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e) {
            this.dataGridView1.Rows[e.RowIndex].HeaderCell.Value = (e.RowIndex + 1).ToString();
            this.dataGridView1.Rows[this.dataGridView1.Rows.Count - 1].HeaderCell.Value = "";
        }
    }
}
