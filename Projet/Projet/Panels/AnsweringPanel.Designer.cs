﻿namespace Projet.Panels {
    partial class AnsweringPanel {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.operationLbl = new System.Windows.Forms.Label();
            this.solveGrb = new System.Windows.Forms.GroupBox();
            this.answerTxt = new System.Windows.Forms.TextBox();
            this.inputsGrb = new System.Windows.Forms.GroupBox();
            this.validateBtn = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.userInputPnl = new Projet.Panels.UserInput();
            this.solveGrb.SuspendLayout();
            this.inputsGrb.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // operationLbl
            // 
            this.operationLbl.AutoSize = true;
            this.operationLbl.Dock = System.Windows.Forms.DockStyle.Right;
            this.operationLbl.Location = new System.Drawing.Point(25, 16);
            this.operationLbl.Name = "operationLbl";
            this.operationLbl.Padding = new System.Windows.Forms.Padding(3);
            this.operationLbl.Size = new System.Drawing.Size(59, 19);
            this.operationLbl.TabIndex = 1;
            this.operationLbl.Text = "Opération";
            this.operationLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // solveGrb
            // 
            this.solveGrb.Controls.Add(this.operationLbl);
            this.solveGrb.Controls.Add(this.answerTxt);
            this.solveGrb.Location = new System.Drawing.Point(191, 82);
            this.solveGrb.Name = "solveGrb";
            this.solveGrb.Size = new System.Drawing.Size(187, 41);
            this.solveGrb.TabIndex = 2;
            this.solveGrb.TabStop = false;
            this.solveGrb.Text = "Résoudre";
            // 
            // answerTxt
            // 
            this.answerTxt.Dock = System.Windows.Forms.DockStyle.Right;
            this.answerTxt.Location = new System.Drawing.Point(84, 16);
            this.answerTxt.Name = "answerTxt";
            this.answerTxt.ReadOnly = true;
            this.answerTxt.Size = new System.Drawing.Size(100, 20);
            this.answerTxt.TabIndex = 2;
            // 
            // inputsGrb
            // 
            this.inputsGrb.AutoSize = true;
            this.inputsGrb.Controls.Add(this.validateBtn);
            this.inputsGrb.Controls.Add(this.userInputPnl);
            this.inputsGrb.Location = new System.Drawing.Point(123, 129);
            this.inputsGrb.Name = "inputsGrb";
            this.inputsGrb.Size = new System.Drawing.Size(322, 308);
            this.inputsGrb.TabIndex = 3;
            this.inputsGrb.TabStop = false;
            // 
            // validateBtn
            // 
            this.validateBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.validateBtn.Location = new System.Drawing.Point(216, 104);
            this.validateBtn.Name = "validateBtn";
            this.validateBtn.Size = new System.Drawing.Size(100, 100);
            this.validateBtn.TabIndex = 1;
            this.validateBtn.Text = "Valider";
            this.validateBtn.UseVisualStyleBackColor = true;
            this.validateBtn.Click += new System.EventHandler(this.validateBtn_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProgressBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 485);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(552, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ProgressBar
            // 
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // userInputPnl
            // 
            this.userInputPnl.AutoSize = true;
            this.userInputPnl.Location = new System.Drawing.Point(6, 19);
            this.userInputPnl.Name = "userInputPnl";
            this.userInputPnl.Size = new System.Drawing.Size(204, 270);
            this.userInputPnl.TabIndex = 0;
            // 
            // AnsweringPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.inputsGrb);
            this.Controls.Add(this.solveGrb);
            this.Name = "AnsweringPanel";
            this.Size = new System.Drawing.Size(552, 507);
            this.Resize += new System.EventHandler(this.AnsweringPanel_Resize);
            this.solveGrb.ResumeLayout(false);
            this.solveGrb.PerformLayout();
            this.inputsGrb.ResumeLayout(false);
            this.inputsGrb.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UserInput userInputPnl;
        private System.Windows.Forms.Label operationLbl;
        private System.Windows.Forms.GroupBox solveGrb;
        private System.Windows.Forms.TextBox answerTxt;
        private System.Windows.Forms.GroupBox inputsGrb;
        private System.Windows.Forms.Button validateBtn;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar ProgressBar;
    }
}
