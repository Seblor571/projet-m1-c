﻿namespace Projet.Panels {
    partial class CorrectionPanel {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.OperationsPnl = new System.Windows.Forms.FlowLayoutPanel();
            this.controlPnl = new System.Windows.Forms.Panel();
            this.repeatBtn = new System.Windows.Forms.Button();
            this.nextOpeBtn = new System.Windows.Forms.Button();
            this.scoreLbl = new System.Windows.Forms.Label();
            this.controlPnl.SuspendLayout();
            this.SuspendLayout();
            // 
            // OperationsPnl
            // 
            this.OperationsPnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OperationsPnl.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.OperationsPnl.Location = new System.Drawing.Point(0, 31);
            this.OperationsPnl.Name = "OperationsPnl";
            this.OperationsPnl.Size = new System.Drawing.Size(419, 407);
            this.OperationsPnl.TabIndex = 0;
            // 
            // controlPnl
            // 
            this.controlPnl.AutoSize = true;
            this.controlPnl.Controls.Add(this.repeatBtn);
            this.controlPnl.Controls.Add(this.nextOpeBtn);
            this.controlPnl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.controlPnl.Location = new System.Drawing.Point(0, 438);
            this.controlPnl.Name = "controlPnl";
            this.controlPnl.Size = new System.Drawing.Size(419, 32);
            this.controlPnl.TabIndex = 0;
            // 
            // repeatBtn
            // 
            this.repeatBtn.AutoSize = true;
            this.repeatBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.repeatBtn.Location = new System.Drawing.Point(171, 6);
            this.repeatBtn.Name = "repeatBtn";
            this.repeatBtn.Size = new System.Drawing.Size(55, 23);
            this.repeatBtn.TabIndex = 1;
            this.repeatBtn.Text = "Répéter";
            this.repeatBtn.UseVisualStyleBackColor = true;
            this.repeatBtn.Click += new System.EventHandler(this.repeatBtn_Click);
            // 
            // nextOpeBtn
            // 
            this.nextOpeBtn.AutoSize = true;
            this.nextOpeBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.nextOpeBtn.Location = new System.Drawing.Point(310, 6);
            this.nextOpeBtn.Name = "nextOpeBtn";
            this.nextOpeBtn.Size = new System.Drawing.Size(106, 23);
            this.nextOpeBtn.TabIndex = 0;
            this.nextOpeBtn.Text = "Opération suivante";
            this.nextOpeBtn.UseVisualStyleBackColor = true;
            this.nextOpeBtn.Click += new System.EventHandler(this.nextOpeBtn_Click);
            // 
            // scoreLbl
            // 
            this.scoreLbl.Dock = System.Windows.Forms.DockStyle.Top;
            this.scoreLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scoreLbl.Location = new System.Drawing.Point(0, 0);
            this.scoreLbl.Name = "scoreLbl";
            this.scoreLbl.Size = new System.Drawing.Size(419, 31);
            this.scoreLbl.TabIndex = 0;
            this.scoreLbl.Text = "Score";
            this.scoreLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CorrectionPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.OperationsPnl);
            this.Controls.Add(this.controlPnl);
            this.Controls.Add(this.scoreLbl);
            this.Name = "CorrectionPanel";
            this.Size = new System.Drawing.Size(419, 470);
            this.Resize += new System.EventHandler(this.CorrectionPanel_Resize);
            this.controlPnl.ResumeLayout(false);
            this.controlPnl.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel OperationsPnl;
        private System.Windows.Forms.Panel controlPnl;
        private System.Windows.Forms.Button nextOpeBtn;
        private System.Windows.Forms.Button repeatBtn;
        private System.Windows.Forms.Label scoreLbl;
    }
}
