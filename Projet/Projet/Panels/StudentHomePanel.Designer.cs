﻿namespace Projet.Panels {
    partial class StudentHomePanel {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.launchTestGrb = new System.Windows.Forms.GroupBox();
            this.progressionStateLbl = new System.Windows.Forms.Label();
            this.progressionLbl = new System.Windows.Forms.Label();
            this.launchTestBtn = new System.Windows.Forms.Button();
            this.answeringPnl = new Projet.Panels.AnsweringPanel();
            this.launchTestGrb.SuspendLayout();
            this.SuspendLayout();
            // 
            // launchTestGrb
            // 
            this.launchTestGrb.Controls.Add(this.progressionStateLbl);
            this.launchTestGrb.Controls.Add(this.progressionLbl);
            this.launchTestGrb.Controls.Add(this.launchTestBtn);
            this.launchTestGrb.Location = new System.Drawing.Point(188, 111);
            this.launchTestGrb.Name = "launchTestGrb";
            this.launchTestGrb.Size = new System.Drawing.Size(200, 133);
            this.launchTestGrb.TabIndex = 0;
            this.launchTestGrb.TabStop = false;
            // 
            // progressionStateLbl
            // 
            this.progressionStateLbl.AutoSize = true;
            this.progressionStateLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressionStateLbl.Location = new System.Drawing.Point(109, 12);
            this.progressionStateLbl.Name = "progressionStateLbl";
            this.progressionStateLbl.Size = new System.Drawing.Size(22, 31);
            this.progressionStateLbl.TabIndex = 2;
            this.progressionStateLbl.Text = "/";
            // 
            // progressionLbl
            // 
            this.progressionLbl.AutoSize = true;
            this.progressionLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressionLbl.Location = new System.Drawing.Point(6, 23);
            this.progressionLbl.Name = "progressionLbl";
            this.progressionLbl.Size = new System.Drawing.Size(97, 18);
            this.progressionLbl.TabIndex = 1;
            this.progressionLbl.Text = "Progression :";
            // 
            // launchTestBtn
            // 
            this.launchTestBtn.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.launchTestBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.launchTestBtn.Location = new System.Drawing.Point(3, 44);
            this.launchTestBtn.Name = "launchTestBtn";
            this.launchTestBtn.Size = new System.Drawing.Size(194, 86);
            this.launchTestBtn.TabIndex = 0;
            this.launchTestBtn.Text = "Lancer le test";
            this.launchTestBtn.UseVisualStyleBackColor = true;
            // 
            // answeringPnl
            // 
            this.answeringPnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.answeringPnl.Location = new System.Drawing.Point(0, 0);
            this.answeringPnl.Name = "answeringPnl";
            this.answeringPnl.Size = new System.Drawing.Size(598, 462);
            this.answeringPnl.TabIndex = 1;
            this.answeringPnl.Visible = false;
            // 
            // StudentHomePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.launchTestGrb);
            this.Controls.Add(this.answeringPnl);
            this.Name = "StudentHomePanel";
            this.Size = new System.Drawing.Size(598, 462);
            this.Resize += new System.EventHandler(this.StudentHomePanel_Resize);
            this.launchTestGrb.ResumeLayout(false);
            this.launchTestGrb.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox launchTestGrb;
        private System.Windows.Forms.Label progressionStateLbl;
        private System.Windows.Forms.Label progressionLbl;
        private AnsweringPanel answeringPnl;
        public System.Windows.Forms.Button launchTestBtn;
    }
}
