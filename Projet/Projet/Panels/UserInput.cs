﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projet.Panels {
    public partial class UserInput : UserControl {

        public event numberButtonPresedsHandler numberButtonPressed;
        public delegate void numberButtonPresedsHandler(int numberPressed);
        public event eraseButtonPresedsHandler eraseButtonPressed;
        public delegate void eraseButtonPresedsHandler();
        public event eraseAllButtonPresedsHandler eraseAllButtonPressed;
        public delegate void eraseAllButtonPresedsHandler();

        public UserInput() {
            InitializeComponent();
        }

        public void enableButtons(bool enable) {
            this.num1Btn.Enabled = enable;
            this.num2Btn.Enabled = enable;
            this.num3Btn.Enabled = enable;
            this.num4Btn.Enabled = enable;
            this.num5Btn.Enabled = enable;
            this.num6Btn.Enabled = enable;
            this.num7Btn.Enabled = enable;
            this.num8Btn.Enabled = enable;
            this.num9Btn.Enabled = enable;
            this.eraseBtn.Enabled = enable;
            this.num0Btn.Enabled = enable;
            this.eraseAllBtn.Enabled = enable;
        }

        // ----- Eventz -----

        /// <summary>
        /// Click sur le bouton 1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void num1Btn_Click(object sender, EventArgs e) {
            numberButtonPressed?.Invoke(1);
        }

        /// <summary>
        /// Click sur le bouton 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void num2Btn_Click(object sender, EventArgs e) {
            numberButtonPressed?.Invoke(2);
        }

        /// <summary>
        /// Click sur le bouton 3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void num3Btn_Click(object sender, EventArgs e) {
            numberButtonPressed?.Invoke(3);
        }

        /// <summary>
        /// Click sur le bouton 4
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void num4Btn_Click(object sender, EventArgs e) {
            numberButtonPressed?.Invoke(4);
        }

        /// <summary>
        /// Click sur le bouton 5
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void num5Btn_Click(object sender, EventArgs e) {
            numberButtonPressed?.Invoke(5);
        }

        /// <summary>
        /// Click sur le bouton 6
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void num6Btn_Click(object sender, EventArgs e) {
            numberButtonPressed?.Invoke(6);
        }

        /// <summary>
        /// Click sur le bouton 7
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void num7Btn_Click(object sender, EventArgs e) {
            numberButtonPressed?.Invoke(7);
        }

        /// <summary>
        /// Click sur le bouton 8
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void num8Btn_Click(object sender, EventArgs e) {
            numberButtonPressed?.Invoke(8);
        }

        /// <summary>
        /// Click sur le bouton 9
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void num9Btn_Click(object sender, EventArgs e) {
            numberButtonPressed?.Invoke(9);
        }

        /// <summary>
        /// Click sur le bouton 0
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void num0Btn_Click(object sender, EventArgs e) {
            numberButtonPressed?.Invoke(0);
        }

        /// <summary>
        /// Click sur le bouton CE
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void eraseBtn_Click(object sender, EventArgs e) {
            eraseButtonPressed?.Invoke();
        }

        /// <summary>
        /// Click sur le bouton C
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void eraseAllBtn_Click(object sender, EventArgs e) {
            eraseAllButtonPressed?.Invoke();
        }
    }
}
