﻿namespace Projet.Panels {
    partial class UserInput {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            this.num1Btn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.eraseAllBtn = new System.Windows.Forms.Button();
            this.num0Btn = new System.Windows.Forms.Button();
            this.eraseBtn = new System.Windows.Forms.Button();
            this.num9Btn = new System.Windows.Forms.Button();
            this.num8Btn = new System.Windows.Forms.Button();
            this.num7Btn = new System.Windows.Forms.Button();
            this.num6Btn = new System.Windows.Forms.Button();
            this.num5Btn = new System.Windows.Forms.Button();
            this.num4Btn = new System.Windows.Forms.Button();
            this.num3Btn = new System.Windows.Forms.Button();
            this.num2Btn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // num1Btn
            // 
            this.num1Btn.Location = new System.Drawing.Point(3, 3);
            this.num1Btn.Name = "num1Btn";
            this.num1Btn.Size = new System.Drawing.Size(60, 60);
            this.num1Btn.TabIndex = 0;
            this.num1Btn.Text = "1";
            this.num1Btn.UseVisualStyleBackColor = true;
            this.num1Btn.Click += new System.EventHandler(this.num1Btn_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.eraseAllBtn, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.num0Btn, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.eraseBtn, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.num9Btn, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.num8Btn, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.num7Btn, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.num6Btn, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.num5Btn, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.num4Btn, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.num3Btn, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.num2Btn, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.num1Btn, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(198, 264);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // eraseAllBtn
            // 
            this.eraseAllBtn.Location = new System.Drawing.Point(135, 201);
            this.eraseAllBtn.Name = "eraseAllBtn";
            this.eraseAllBtn.Size = new System.Drawing.Size(60, 60);
            this.eraseAllBtn.TabIndex = 11;
            this.eraseAllBtn.Text = "C";
            this.eraseAllBtn.UseVisualStyleBackColor = true;
            this.eraseAllBtn.Click += new System.EventHandler(this.eraseAllBtn_Click);
            // 
            // num0Btn
            // 
            this.num0Btn.Location = new System.Drawing.Point(69, 201);
            this.num0Btn.Name = "num0Btn";
            this.num0Btn.Size = new System.Drawing.Size(60, 60);
            this.num0Btn.TabIndex = 10;
            this.num0Btn.Text = "0";
            this.num0Btn.UseVisualStyleBackColor = true;
            this.num0Btn.Click += new System.EventHandler(this.num0Btn_Click);
            // 
            // eraseBtn
            // 
            this.eraseBtn.Location = new System.Drawing.Point(3, 201);
            this.eraseBtn.Name = "eraseBtn";
            this.eraseBtn.Size = new System.Drawing.Size(60, 60);
            this.eraseBtn.TabIndex = 9;
            this.eraseBtn.Text = "CE";
            this.eraseBtn.UseVisualStyleBackColor = true;
            this.eraseBtn.Click += new System.EventHandler(this.eraseBtn_Click);
            // 
            // num9Btn
            // 
            this.num9Btn.Location = new System.Drawing.Point(135, 135);
            this.num9Btn.Name = "num9Btn";
            this.num9Btn.Size = new System.Drawing.Size(60, 60);
            this.num9Btn.TabIndex = 8;
            this.num9Btn.Text = "9";
            this.num9Btn.UseVisualStyleBackColor = true;
            this.num9Btn.Click += new System.EventHandler(this.num9Btn_Click);
            // 
            // num8Btn
            // 
            this.num8Btn.Location = new System.Drawing.Point(69, 135);
            this.num8Btn.Name = "num8Btn";
            this.num8Btn.Size = new System.Drawing.Size(60, 60);
            this.num8Btn.TabIndex = 7;
            this.num8Btn.Text = "8";
            this.num8Btn.UseVisualStyleBackColor = true;
            this.num8Btn.Click += new System.EventHandler(this.num8Btn_Click);
            // 
            // num7Btn
            // 
            this.num7Btn.Location = new System.Drawing.Point(3, 135);
            this.num7Btn.Name = "num7Btn";
            this.num7Btn.Size = new System.Drawing.Size(60, 60);
            this.num7Btn.TabIndex = 6;
            this.num7Btn.Text = "7";
            this.num7Btn.UseVisualStyleBackColor = true;
            this.num7Btn.Click += new System.EventHandler(this.num7Btn_Click);
            // 
            // num6Btn
            // 
            this.num6Btn.Location = new System.Drawing.Point(135, 69);
            this.num6Btn.Name = "num6Btn";
            this.num6Btn.Size = new System.Drawing.Size(60, 60);
            this.num6Btn.TabIndex = 5;
            this.num6Btn.Text = "6";
            this.num6Btn.UseVisualStyleBackColor = true;
            this.num6Btn.Click += new System.EventHandler(this.num6Btn_Click);
            // 
            // num5Btn
            // 
            this.num5Btn.Location = new System.Drawing.Point(69, 69);
            this.num5Btn.Name = "num5Btn";
            this.num5Btn.Size = new System.Drawing.Size(60, 60);
            this.num5Btn.TabIndex = 4;
            this.num5Btn.Text = "5";
            this.num5Btn.UseVisualStyleBackColor = true;
            this.num5Btn.Click += new System.EventHandler(this.num5Btn_Click);
            // 
            // num4Btn
            // 
            this.num4Btn.Location = new System.Drawing.Point(3, 69);
            this.num4Btn.Name = "num4Btn";
            this.num4Btn.Size = new System.Drawing.Size(60, 60);
            this.num4Btn.TabIndex = 3;
            this.num4Btn.Text = "4";
            this.num4Btn.UseVisualStyleBackColor = true;
            this.num4Btn.Click += new System.EventHandler(this.num4Btn_Click);
            // 
            // num3Btn
            // 
            this.num3Btn.Location = new System.Drawing.Point(135, 3);
            this.num3Btn.Name = "num3Btn";
            this.num3Btn.Size = new System.Drawing.Size(60, 60);
            this.num3Btn.TabIndex = 2;
            this.num3Btn.Text = "3";
            this.num3Btn.UseVisualStyleBackColor = true;
            this.num3Btn.Click += new System.EventHandler(this.num3Btn_Click);
            // 
            // num2Btn
            // 
            this.num2Btn.Location = new System.Drawing.Point(69, 3);
            this.num2Btn.Name = "num2Btn";
            this.num2Btn.Size = new System.Drawing.Size(60, 60);
            this.num2Btn.TabIndex = 1;
            this.num2Btn.Text = "2";
            this.num2Btn.UseVisualStyleBackColor = true;
            this.num2Btn.Click += new System.EventHandler(this.num2Btn_Click);
            // 
            // UserInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UserInput";
            this.Size = new System.Drawing.Size(204, 270);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button num1Btn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button eraseAllBtn;
        private System.Windows.Forms.Button num0Btn;
        private System.Windows.Forms.Button eraseBtn;
        private System.Windows.Forms.Button num9Btn;
        private System.Windows.Forms.Button num8Btn;
        private System.Windows.Forms.Button num7Btn;
        private System.Windows.Forms.Button num6Btn;
        private System.Windows.Forms.Button num5Btn;
        private System.Windows.Forms.Button num4Btn;
        private System.Windows.Forms.Button num3Btn;
        private System.Windows.Forms.Button num2Btn;
    }
}
