﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet.Panels {
    /// <summary>
    /// Interface implémentant une fonction de réinitialisation de panel
    /// </summary>
    interface PanelInterface {

        /// <summary>
        /// Réinitialisation d'un panel
        /// </summary>
        void cleanPanel();

    }
}
