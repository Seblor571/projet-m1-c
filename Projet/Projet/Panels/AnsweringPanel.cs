﻿using System;
using System.Windows.Forms;

namespace Projet.Panels {
    public partial class AnsweringPanel : UserControl, PanelInterface {

        /// <summary>
        /// Nombre d'opérations (constante de test uniquement)
        /// </summary>
        private static int NUMBER_OF_OPERATIONS = 20;

        /// <summary>
        /// Durée de l'animation de changement d'opération (en ms)
        /// </summary>
        private static int ANIMATION_DURATION = 250;

        /// <summary>
        /// Évènement de la fin du test
        /// </summary>
        public event testFinishedHandler testFinished;

        /// <summary>
        /// Délégué de la fin du test
        /// </summary>
        public delegate void testFinishedHandler(Operation[] operations, int[] answers);

        /// <summary>
        /// Liste des opérations du test
        /// </summary>
        public Operation[] operations = null;

        /// <summary>
        /// Liste des réponses données
        /// </summary>
        public int[] answers;

        /// <summary>
        /// Identifiant de l'opération courrante
        /// </summary>
        private int currentOperation = 0;

        /// <summary>
        /// Timer de l'animation de changement d'opération
        /// </summary>
        Timer animationTimer = new Timer();

        /// <summary>
        /// Temps écoulé pour l'animation de changement d'opération
        /// </summary>
        double animationTimeSpan;

        public AnsweringPanel() {
            InitializeComponent();
            this.userInputPnl.numberButtonPressed += addDigit;
            this.userInputPnl.eraseButtonPressed += removeLastDigit;
            this.userInputPnl.eraseAllButtonPressed += removeAllDigits;

            animationTimer.Interval = 10;
            animationTimer.Tick += animationTickBegin;
        }

        public void cleanPanel() {
            this.operationLbl.Text = "Opération";
            this.validateBtn.Text = "Valider";
            this.answerTxt.Text = "";
            this.ProgressBar.Value = 0;
            operations = null;
        }

        /// <summary>
        /// Démarrage du test (génération des opréations)
        /// </summary>
        public void startTest() {
            if (operations == null) {
                currentOperation = 0;
                operations = Operation.generateAllOperations(Session.Instance.profile._level, Session.Instance.profile._transition);
                answers = new int[20];

                this.operationLbl.Text = operations[currentOperation].ToStringWithoutAnswer();
            }

            // Synthetizing operation
            operations[currentOperation].toSpeech();
        }
        
        // ----- Events -----

        /// <summary>
        /// Redimentionnement du panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void AnsweringPanel_Resize(object sender, EventArgs e) {
            this.inputsGrb.Left = (this.ClientSize.Width - this.inputsGrb.Width) / 2;
            this.inputsGrb.Top = (this.ClientSize.Height - this.inputsGrb.Height) / 2;
            this.solveGrb.Left = (this.ClientSize.Width - this.solveGrb.Width) / 2;
            this.solveGrb.Top = (this.ClientSize.Height - this.userInputPnl.Height - this.solveGrb.Height) / 4;
        }

        /// <summary>
        /// Efface le dernier chiffre de la solution
        /// </summary>
        private void removeLastDigit() {
            if (this.answerTxt.Text != string.Empty) {
                this.answerTxt.Text = this.answerTxt.Text.Remove(answerTxt.Text.Length - 1);
            }
        }

        /// <summary>
        /// Efface la solution complète
        /// </summary>
        private void removeAllDigits() {
            this.answerTxt.Clear();
        }

        /// <summary>
        /// Ajoute un chiffre à la solution
        /// </summary>
        /// <param name="number"></param>
        private void addDigit(int number) {
            this.answerTxt.Text += number.ToString();
        }

        /// <summary>
        /// Click sur le bouton de validation de l'opération
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void validateBtn_Click(object sender, EventArgs e) {
            if (this.answerTxt.Text == string.Empty) {
                return;
            }

            answers[currentOperation] = int.Parse(this.answerTxt.Text);

            if (currentOperation == NUMBER_OF_OPERATIONS - 2) {
                this.validateBtn.Text = "Terminer le test";
            }
            else if (currentOperation == NUMBER_OF_OPERATIONS - 1) {
                testFinished.Invoke(operations, answers);
                this.cleanPanel();
                return;
            }
            nextOperation();
        }

        /// <summary>
        /// Changes operation (Animation)
        /// </summary>
        private void nextOperation() {

            this.ProgressBar.Value += 5;

            currentOperation++;

            animationTimeSpan = 0;
            animationTimer.Start();

            // Desactivating buttons
            this.userInputPnl.enableButtons(false);
            this.validateBtn.Enabled = false;
        }

        // ----- Animation -----

        /// <summary>
        /// Pour chaque tick de début de l'animation de changement d'opération
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void animationTickBegin(object sender, EventArgs e) {

            // Animation first part
            if (animationTimeSpan < ANIMATION_DURATION / 2) {
                this.solveGrb.Left = Convert.ToInt32(Math.Round(Tools.easeInQuad(animationTimeSpan, (this.ClientSize.Width - this.solveGrb.Width) / 2, this.ClientSize.Width, ANIMATION_DURATION / 2)));
                animationTimeSpan += 10;
            }

            // Animation second part
            else {
                animationTimeSpan = 0;

                this.answerTxt.Clear();

                // Changing tick function
                animationTimer.Tick -= animationTickBegin;
                animationTimer.Tick += animationTickEnd;

                this.operationLbl.Text = operations[currentOperation].ToStringWithoutAnswer();
            }
        }

        /// <summary>
        /// Pour chaque tick de fin de l'animation de changement d'opération
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void animationTickEnd(object sender, EventArgs e) {
            this.solveGrb.Left = Convert.ToInt32(Math.Round(Tools.easeOutQuad(animationTimeSpan, -this.solveGrb.Width, this.solveGrb.Width + (this.ClientSize.Width - this.solveGrb.Width) / 2, ANIMATION_DURATION / 2)));
            animationTimeSpan += 10;
            if (animationTimeSpan >= ANIMATION_DURATION / 2) {
                animationTimer.Stop();

                // Assuring the final position
                this.solveGrb.Left = (this.ClientSize.Width - this.solveGrb.Width) / 2;

                // changing tick function
                animationTimer.Tick -= animationTickEnd;
                animationTimer.Tick += animationTickBegin;

                // Reactivating buttons
                this.userInputPnl.enableButtons(true);
                this.validateBtn.Enabled = true;

                // Synthetizing operation
                operations[currentOperation].toSpeech();
            }

        }
    }

}
