﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projet.Panels {
    /// <summary>
    /// Panneau de l'élève
    /// </summary>
    public partial class StudentHomePanel : UserControl, PanelInterface {
        public StudentHomePanel() {
            InitializeComponent();
        }

        /// <summary>
        /// Affiche une flèche correspondant à l'évolution des scores de l'utilisateur courant
        /// </summary>
        public void loadSessionStats() {
            Console.WriteLine(Database.requestEvolution(Session.Instance.profile._username));
            switch (Database.requestEvolution(Session.Instance.profile._username)) {
                case 1:
                    this.progressionStateLbl.Text = "😃";
                    break;
                case 0:
                    this.progressionStateLbl.Text = "😊";
                    break;
                case -1:
                    this.progressionStateLbl.Text = "😕";
                    break;
                default:
                    this.progressionStateLbl.Text = "NA";
                    break;
            }
        }

        public void cleanPanel() {
            this.loadSessionStats();
            this.launchTestBtn.Text = "Lancer le test";
        }

        // ----- Events -----

        /// <summary>
        /// Redimentionnement du panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StudentHomePanel_Resize(object sender, EventArgs e) {
            this.launchTestGrb.Left = (this.ClientSize.Width - this.launchTestGrb.Width) / 2;
            this.launchTestGrb.Top = (this.ClientSize.Height - this.launchTestGrb.Height) / 4;
        }
    }
}
