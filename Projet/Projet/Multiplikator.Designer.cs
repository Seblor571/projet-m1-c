﻿using System.Drawing;
using System.Windows.Forms;

namespace Projet {
    partial class Multiplikator {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Multiplikator));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.frontPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disconnectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginPnl = new Projet.Panels.LoginProfilePanel();
            this.createProfilePnl = new Projet.Panels.CreateProfilePanel();
            this.adminPnl = new Projet.Panels.AdminPanel();
            this.studentHomePnl = new Projet.Panels.StudentHomePanel();
            this.answeringPnl = new Projet.Panels.AnsweringPanel();
            this.correctionPnl = new Projet.Panels.CorrectionPanel();
            this.statisticsPnl = new Projet.Panels.StatisticsPanel();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(713, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadProfileToolStripMenuItem,
            this.newProfileToolStripMenuItem,
            this.frontPageToolStripMenuItem,
            this.disconnectToolStripMenuItem});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.fichierToolStripMenuItem.Text = "Fichier";
            this.fichierToolStripMenuItem.Visible = false;
            // 
            // loadProfileToolStripMenuItem
            // 
            this.loadProfileToolStripMenuItem.Name = "loadProfileToolStripMenuItem";
            this.loadProfileToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.loadProfileToolStripMenuItem.Text = "Charger un profil...";
            this.loadProfileToolStripMenuItem.Visible = false;
            this.loadProfileToolStripMenuItem.Click += new System.EventHandler(this.chargerUnProfilToolStripMenuItem_Click);
            // 
            // newProfileToolStripMenuItem
            // 
            this.newProfileToolStripMenuItem.Name = "newProfileToolStripMenuItem";
            this.newProfileToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.newProfileToolStripMenuItem.Text = "Nouveau profil";
            this.newProfileToolStripMenuItem.Visible = false;
            this.newProfileToolStripMenuItem.Click += new System.EventHandler(this.newProfileToolStripMenuItem_Click);
            // 
            // frontPageToolStripMenuItem
            // 
            this.frontPageToolStripMenuItem.Name = "frontPageToolStripMenuItem";
            this.frontPageToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.frontPageToolStripMenuItem.Text = "Accueil";
            this.frontPageToolStripMenuItem.Click += new System.EventHandler(this.frontPageToolStripMenuItem_Click);
            // 
            // disconnectToolStripMenuItem
            // 
            this.disconnectToolStripMenuItem.Name = "disconnectToolStripMenuItem";
            this.disconnectToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.disconnectToolStripMenuItem.Text = "Déconnexion";
            this.disconnectToolStripMenuItem.Click += new System.EventHandler(this.disconnectToolStripMenuItem_Click);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // loginPnl
            // 
            this.loginPnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginPnl.Location = new System.Drawing.Point(0, 24);
            this.loginPnl.Name = "loginPnl";
            this.loginPnl.Size = new System.Drawing.Size(713, 512);
            this.loginPnl.TabIndex = 2;
            // 
            // createProfilePnl
            // 
            this.createProfilePnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.createProfilePnl.Location = new System.Drawing.Point(0, 24);
            this.createProfilePnl.Name = "createProfilePnl";
            this.createProfilePnl.Size = new System.Drawing.Size(713, 512);
            this.createProfilePnl.TabIndex = 1;
            this.createProfilePnl.Visible = false;
            // 
            // adminPnl
            // 
            this.adminPnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.adminPnl.Location = new System.Drawing.Point(0, 0);
            this.adminPnl.Name = "adminPnl";
            this.adminPnl.Size = new System.Drawing.Size(713, 536);
            this.adminPnl.TabIndex = 4;
            this.adminPnl.Visible = false;
            // 
            // studentHomePnl
            // 
            this.studentHomePnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.studentHomePnl.Location = new System.Drawing.Point(0, 0);
            this.studentHomePnl.Name = "studentHomePnl";
            this.studentHomePnl.Size = new System.Drawing.Size(713, 536);
            this.studentHomePnl.TabIndex = 5;
            this.studentHomePnl.Visible = false;
            // 
            // answeringPnl
            // 
            this.answeringPnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.answeringPnl.Location = new System.Drawing.Point(0, 0);
            this.answeringPnl.Name = "answeringPnl";
            this.answeringPnl.Size = new System.Drawing.Size(713, 536);
            this.answeringPnl.TabIndex = 6;
            this.answeringPnl.Visible = false;
            // 
            // correctionPnl
            // 
            this.correctionPnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.correctionPnl.Location = new System.Drawing.Point(0, 0);
            this.correctionPnl.Name = "correctionPnl";
            this.correctionPnl.Size = new System.Drawing.Size(713, 536);
            this.correctionPnl.TabIndex = 7;
            // 
            // statisticsPnl
            // 
            this.statisticsPnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statisticsPnl.Location = new System.Drawing.Point(0, 24);
            this.statisticsPnl.Name = "statisticsPnl";
            this.statisticsPnl.Size = new System.Drawing.Size(713, 512);
            this.statisticsPnl.TabIndex = 8;
            // 
            // Multiplikator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 536);
            this.Controls.Add(this.loginPnl);
            this.Controls.Add(this.statisticsPnl);
            this.Controls.Add(this.createProfilePnl);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.adminPnl);
            this.Controls.Add(this.studentHomePnl);
            this.Controls.Add(this.answeringPnl);
            this.Controls.Add(this.correctionPnl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(575, 575);
            this.Name = "Multiplikator";
            this.Text = "Multiplikator2000";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private ToolStripMenuItem quitterToolStripMenuItem;
        private Panels.CreateProfilePanel createProfilePnl;
        private Panels.LoginProfilePanel loginPnl;
        private ToolStripMenuItem disconnectToolStripMenuItem;
        public ToolStripMenuItem loadProfileToolStripMenuItem;
        public ToolStripMenuItem newProfileToolStripMenuItem;
        private Panels.AdminPanel adminPnl;
        private ToolStripMenuItem frontPageToolStripMenuItem;
        private Panels.StudentHomePanel studentHomePnl;
        private Panels.AnsweringPanel answeringPnl;
        private Panels.CorrectionPanel correctionPnl;
        private Panels.StatisticsPanel statisticsPnl;
    }
}

