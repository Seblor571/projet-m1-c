﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet {
    class Database {

        /// <summary>
        /// Mot de passe de la base de données (inutile pour le moment)
        /// </summary>
        public static string DB_PASSWORD = "Rantanplan";

        /// <summary>
        /// Connection statique à la base de données
        /// </summary>
        public static SQLiteConnection dbConnection;

        /// <summary>
        /// Readonly : Chemin vers les fichiers locaux de l'application (contient un "\" final)
        /// </summary>
        public static string DATABASE_CREATION {
            get {
                return @"
--
-- Structure de la table `SCORES`
--

CREATE TABLE `scores` (
  `username` varchar(256) NOT NULL,
  `level` varchar(64) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `score` int(11) NOT NULL,
  `nbOpeSup` int(11) NOT NULL,
  `succOpeSup` int(11) NOT NULL
)";
            }
        }

        /// <summary>
        /// Charge et créé la base de données si elle n'existe pas
        /// </summary>
        public static void loadDatabase() {


            try {
                //dbConnection = new SQLiteConnection("Data Source=" + Settings.APPDATAPATH + "database.mkt" + ";Version=3;Password=" + DB_PASSWORD + ";");
                dbConnection = new SQLiteConnection("Data Source=" + Settings.APPDATAPATH + "database.mkt" + ";Version=3;");
                dbConnection.Open();
            }
            catch (Exception) {
                // If file is not readable, recreate it
                dbConnection.Close();
                dbConnection.Dispose();
                createDatabase();
            }

        }

        /// <summary>
        /// Crée la base de données (et ouvre la connexion)
        /// </summary>
        public static void createDatabase() {
            SQLiteConnection.CreateFile(Settings.APPDATAPATH + "database.mkt");
            dbConnection = new SQLiteConnection("Data Source=" + Settings.APPDATAPATH + "database.mkt" + ";Version=3;");
            //dbConnection.SetPassword(DB_PASSWORD);
            dbConnection.Open();

            SQLiteCommand command = new SQLiteCommand(DATABASE_CREATION, dbConnection);
            command.ExecuteNonQuery();
            dbConnection.Close();
        }

        /// <summary>
        /// Insertion d'un résultat dans la base de données
        /// </summary>
        /// <param name="username">Nom de l'élève</param>
        /// <param name="level">niveau de base du test</param>
        /// <param name="score">score final (sur 20)</param>
        /// <param name="nbOpeSup">nombre d'opération du niveau suivant (dépend du niveau de transition) (Défaut : 0)</param>
        /// <param name="succOpeSup">nombre d'opération réussies du niveau suivant (Défaut : 0)</param>
        public static void insertResult(string username, Levels level, int score, int nbOpeSup = 0, int succOpeSup = 0) {
            string ins = @"INSERT INTO SCORES (username, level, score, nbOpeSup, succOpeSup) VALUES (@username, @level, @score, @nbOpeSup, @succOpeSup);";
            SQLiteCommand command = new SQLiteCommand(ins, dbConnection);
            command.Parameters.AddWithValue("@username", username);
            command.Parameters.AddWithValue("@level", level);
            command.Parameters.AddWithValue("@score", score);
            command.Parameters.AddWithValue("@nbOpeSup", nbOpeSup);
            command.Parameters.AddWithValue("@succOpeSup", succOpeSup);
            command.ExecuteNonQuery();
        }

        /// <summary>
        /// Récupère la progression de l'utilisateur entre la moyenne des 3 derniers tests et des 5 suivants
        /// </summary>
        /// <param name="username"></param>
        /// <returns>Renvoie un entier, ou null si base de données vide (ou erreur)</returns>
        public static int? requestEvolution(string username) {

            string req = @"
SELECT avg(newest) - avg(recent)
FROM (SELECT score AS newest
				FROM SCORES
				WHERE username=@username
				ORDER BY date DESC
				LIMIT 3),
	(SELECT score AS recent
				FROM SCORES
				WHERE username=@username
				ORDER BY date DESC
				LIMIT 5 OFFSET 3);";
            SQLiteCommand command = new SQLiteCommand(req, dbConnection);
            command.Parameters.AddWithValue("@username", username);
            SQLiteDataReader re = command.ExecuteReader();
            float res;
            try {
                while (re.Read()) {
                    object obj = re[0];
                    res = float.Parse(re[0].ToString());
                    return Math.Sign(float.Parse(res.ToString()));
                }
            }
            catch (Exception) {
                return null;
            }
            return 0;
        }

        /// <summary>
        /// Récupère les derniers scores d'un utilisateur
        /// </summary>
        /// <param name="username"></param>
        /// <param name="numberOfResults"></param>
        /// <param name="level"></param>
        /// <returns>Renvoie la liste des derniers scores, ou null si pas assez de résultats</returns>
        public static List<int> requestLastScores(string username, int numberOfResults, Levels level) {

            string req = @"
SELECT score
FROM SCORES
WHERE username=@username
AND level=@level
ORDER BY date DESC
LIMIT @limit;";
            SQLiteCommand command = new SQLiteCommand(req, dbConnection);
            command.Parameters.AddWithValue("@username", username);
            command.Parameters.AddWithValue("@level", level);
            command.Parameters.AddWithValue("@limit", numberOfResults);
            SQLiteDataReader re = command.ExecuteReader();

            List<int> scores = new List<int>();
            try {
                while (re.Read()) {
                    scores.Add(int.Parse(re[0].ToString()));
                }
            }
            catch (Exception) {
                return null;
            }

            if (scores.Count < numberOfResults) {
                // returning null is not enough results
                return null;
            }
            else {
                return scores;
            }
        }

        /// <summary>
        /// Récupère les derniers résultats d'un utilisateur
        /// </summary>
        /// <param name="username"></param>
        /// <param name="numberOfResults">Nombre de résultats maximim attendus (défaut : 15)</param>
        /// <param name="chronological">Ordre chronologique : les premiers éléments de la liste seront les plus ancient (true par défaut)</param>
        /// <returns>Renvoie la liste des derniers résultats</returns>
        public static List<TestResult> requestLastResults(string username, int numberOfResults = 0, bool chronological = true) {

            SQLiteCommand command = null;
            string req;

            if (numberOfResults <= 0) {
                // If no limits specified

                req = @"
SELECT date, score, level, nbOpeSup, succOpeSup
FROM SCORES
WHERE username=@username
ORDER BY date DESC;";
                command = new SQLiteCommand(req, dbConnection);
                command.Parameters.AddWithValue("@limit", numberOfResults);
            }
            else {
                // If limit specified
                req = @"
SELECT date, score, level, nbOpeSup, succOpeSup
FROM SCORES
WHERE username=@username
ORDER BY date DESC
LIMIT @limit;";
                command = new SQLiteCommand(req, dbConnection);
            }

            command.Parameters.AddWithValue("@username", username);
            SQLiteDataReader re = command.ExecuteReader();

            List<TestResult> results = new List<TestResult>();
            try {
                while (re.Read()) {
                    DateTime date = DateTime.Parse(re[0].ToString());


                    results.Add(new TestResult(date,
                        int.Parse(re[1].ToString()),
                        Level.getLevelFromInt(int.Parse(re[2].ToString())),
                        int.Parse(re[3].ToString()),
                        int.Parse(re[4].ToString())));
                }
            }
            catch (Exception e) {
                Console.WriteLine(e);
                return null;
            }

            if (chronological) {
                results.Reverse();
            }

            return results;
        }

        /// <summary>
        /// Ajoute un jeu de test dans la base de données pour l'utilisateur indiqué
        /// </summary>
        /// <param name="username"></param>
        public static void addPlayTest(string username) {
            string ins = @"INSERT INTO `scores` VALUES (@username,'1','2017-04-15 16:20:27',19,10,9);
INSERT INTO `scores` VALUES (@username,'1','2017-04-15 16:20:12',17,10,7);
INSERT INTO `scores` VALUES (@username,'1','2017-04-15 16:19:41',17,10,7);
INSERT INTO `scores` VALUES (@username,'1','2017-04-15 16:18:33',15,10,6);
INSERT INTO `scores` VALUES (@username,'1','2017-04-15 16:18:21',19,5,5);
INSERT INTO `scores` VALUES (@username,'1','2017-04-15 01:54:13',18,5,4);
INSERT INTO `scores` VALUES (@username,'1','2017-04-15 01:51:34',17,5,3);
INSERT INTO `scores` VALUES (@username,'1','2017-04-15 01:27:49',20,0,0);
INSERT INTO `scores` VALUES (@username,'1','2017-04-15 01:00:33',19,0,0);
INSERT INTO `scores` VALUES (@username,'1','2017-04-15 00:58:44',17,0,0);
INSERT INTO `scores` VALUES (@username,'0','2017-04-13 15:41:57',20,15,15);
INSERT INTO `scores` VALUES (@username,'0','2017-04-13 15:41:50',18,15,14);
INSERT INTO `scores` VALUES (@username,'0','2017-04-13 15:40:14',17,15,12);
INSERT INTO `scores` VALUES (@username,'0','2017-04-13 15:40:08',20,10,10);
INSERT INTO `scores` VALUES (@username,'0','2017-04-13 15:39:59',17,10,7);
INSERT INTO `scores` VALUES (@username,'0','2017-04-13 15:39:36',17,10,8);
INSERT INTO `scores` VALUES (@username,'0','2017-04-13 15:28:40',16,10,7);
INSERT INTO `scores` VALUES (@username,'0','2017-04-13 15:22:17',12,10,2);
INSERT INTO `scores` VALUES (@username,'0','2017-02-14 22:10:27',18,5,3);
INSERT INTO `scores` VALUES (@username,'0','2017-02-14 21:10:27',19,5,4);
INSERT INTO `scores` VALUES (@username,'0','2017-02-14 20:10:27',18,5,3);
INSERT INTO `scores` VALUES (@username,'0','2017-02-14 19:10:27',18,5,3);
INSERT INTO `scores` VALUES (@username,'0','2017-02-14 18:10:27',16,5,2);
INSERT INTO `scores` VALUES (@username,'0','2017-02-14 16:10:27',18,0,0);
INSERT INTO `scores` VALUES (@username,'0','2017-02-14 00:56:40',17,0,0);
INSERT INTO `scores` VALUES (@username,'0','2017-02-14 00:55:35',15,0,0);
INSERT INTO `scores` VALUES (@username,'0','2017-02-14 00:50:13',10,0,0);
CREATE UNIQUE INDEX `date` ON `scores` (`date` );";
            SQLiteCommand command = new SQLiteCommand(ins, dbConnection);
            command.Parameters.AddWithValue("@username", username);

            // Using try-catch because of UNIQUE value for date
            try {
                command.ExecuteNonQuery();
            }
            catch (Exception) {
            }
        }
    }
}
