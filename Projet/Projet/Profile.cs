﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Projet {
    /// <summary>
    /// Profil d'un utilisateur (Administrateur, Superviseur ou Élève)
    /// </summary>
    [Serializable()]
    [XmlRoot("profile")]
    public class Profile {

        /// <summary>
        /// Nom de l'utilisateur (correspond au nom du fichier xml sans l'extension)
        /// </summary>
        [XmlElement("username")]
        public string _username;

        /// <summary>
        /// Mot de passe (chiffré en SHA1)
        /// </summary>
        [XmlElement("password")]
        public string _password;

        /// <summary>
        /// Nom de l'utilisateur
        /// </summary>
        [XmlElement("nom")]
        public string _name;

        /// <summary>
        /// Prénom de l'utilisateur
        /// </summary>
        [XmlElement("prenom")]
        public string _firstname;

        /// <summary>
        /// Niveau de l'utilisateur
        /// </summary>
        [XmlElement("niveau")]
        public Levels _level;

        /// <summary>
        /// État de transition de l'utilisateur
        /// </summary>
        [XmlElement("transition")]
        public int _transition;

        /// <summary>
        /// Correspond à la possibilité d'évolution du profil
        /// </summary>
        [XmlElement("evolving")]
        public bool _evolving;

        /// <summary>
        /// Constructeur de Profile
        /// </summary>
        /// <param name="username">Nom de l'utilisateur (l'unicité doit être vérifiée avant construction)</param>
        /// <param name="password">Mot de passe en clair (sera chiffré en SHA1)</param>
        /// <param name="nom">Nom de l'élève</param>
        /// <param name="prenom">Prénom de l'élève</param>
        /// <param name="level">Niveau de l'élève</param>
        /// <param name="transition">État de transition (0 par défaut)</param>
        /// <param name="evolving">Possibilité d'évolution du profil (activé par défaut)</param>
        public Profile(string username, string password, string nom, string prenom, Levels level, int transition = 0, Boolean evolving = true) {
            this._username = username;
            this._password = Tools.sha1Encode(password);
            this._name = nom;
            this._firstname = prenom;
            this._level = level;
            this._transition = transition;
            this._evolving = evolving;
        }

        public Profile() {
            this._username = "";
            this._password = "";
            this._name = "";
            this._firstname = "";
            this._level = Levels.LEVEL1_1;
            this._transition = 0;
            this._evolving = true;
        }

        /// <summary>
        /// Sauvegarde le profil dans le dossier de profils
        /// </summary>
        public void saveProfile() {
            Tools.Save(this, Settings.PROFILESPATH + this._username + ".xml");
            // Reloading all profiles
            Tools.loadProfiles();
        }

        // ----- ShouldSerialize -----
        /// <summary>
        /// Retourne False pour les meta-users (Administrateur & Superviseur)
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerialize_name() {
            return (!UserType.isMeta(this));
        }

        /// <summary>
        /// Retourne False pour les meta-users (Administrateur & Superviseur)
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerialize_firstname() {
            return (!UserType.isMeta(this));
        }

        /// <summary>
        /// Retourne False pour les meta-users (Administrateur & Superviseur)
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerialize_level() {
            return (!UserType.isMeta(this));
        }

        /// <summary>
        /// Retourne False pour les meta-users (Administrateur & Superviseur)
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerialize_transition() {
            return (!UserType.isMeta(this));
        }

        /// <summary>
        /// Retourne False pour les meta-users (Administrateur & Superviseur)
        /// </summary>
        /// <returns></returns>
        public bool ShouldSerialize_evolving() {
            return (!UserType.isMeta(this));
        }

        // ----- Methodes -----

        /// <summary>
        /// Fonction de formattage pour le texte de ComboBox
        /// </summary>
        /// <returns>Chaîne de carachère générée suivant le format suivant : [nom d'utilisateur] ([Prénom] [NOM])</returns>
        public string toLoginString() {
            if (UserType.isMeta(this)) {
                return this._username;
            }
            return String.Format("{0} ({1} {2})", this._username, this._firstname, this._name.ToUpper());
        }

        /// <summary>
        /// décrémente l'état de transition (Note : devrait être inutile dans ce projet)
        /// </summary>
        public void decrementLevel() {
            if (this._level == Levels.LEVEL1_1 && this._transition == 0) {
                return;
            }
            if (this._transition == 0) {
                this._level--;
                this._transition = Settings.Instance.nbTransitionState;
            }
            else {
                this._transition--;
            }
            this.saveProfile();
        }

        /// <summary>
        /// Incrémente l'état de transition
        /// </summary>
        /// <returns>Renvoie True si tous les niveaux ont été réussis</returns>
        public bool incrementLevel() {
            if (this._level == Levels.LEVEL3) {
                return true;
            }
            if (this._transition == Settings.Instance.nbTransitionState) {
                this._level++;
                this._transition = 0;
            }
            else {
                this._transition++;
            }
            this.saveProfile();
            return false;
        }
    }
}
