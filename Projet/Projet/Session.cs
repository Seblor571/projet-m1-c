﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Projet {
    /// <summary>
    /// Singleton: Correspond à la session courante
    /// </summary>
    public class Session {

        /// <summary>
        /// Instance du Singleton
        /// </summary>
        private static Session _instance;

        /// <summary>
        /// Type de l'utilisateur
        /// </summary>
        private UserTypes _userType;

        /// <summary>
        /// Page d'accueil (dépend de l'utilisateur)
        /// </summary>
        private UserControl _frontPage;

        /// <summary>
        /// Variable stockant le profile chargé
        /// </summary>
        private Profile _profile;

        private Session() { }

        /// <summary>
        /// Recrée une nouvelle Session
        /// </summary>
        public static void clearSession() {
            _instance = new Session();
        }

        // ----- Accessors -----

        public Profile profile {
            get {
                return this._profile;
            }
            set {
                if (value._username == UserTypes.ADMINISTRATOR.getName())
                    this._userType = UserTypes.ADMINISTRATOR;
                else if (value._username == UserTypes.SUPERVISOR.getName())
                    this._userType = UserTypes.SUPERVISOR;
                else
                    this._userType = UserTypes.STUDENT;

                this._profile = value;
            }
        }


        /// <summary>
        /// Renvoie True si la session courante est une session d'Administrateur
        /// </summary>
        public bool isAdmin {
            get {
                return this._userType == UserTypes.ADMINISTRATOR;
            }
        }

        /// <summary>
        /// Renvoie True si la session courante est une session de Superviseur
        /// </summary>
        public bool isSupervisor {
            get {
                return this._userType == UserTypes.SUPERVISOR;
            }
        }

        /// <summary>
        /// Récupère la page d'accueil
        /// </summary>
        public UserControl frontPage {
            get {
                return this._frontPage;
            }
            set {
                this._frontPage = value;
            }
        }

        /// <summary>
        /// Récupère l'instance du Singleton
        /// </summary>
        public static Session Instance {
            get {
                if (_instance == null) {
                    _instance = new Session();
                }
                return _instance;
            }
        }
    }
}