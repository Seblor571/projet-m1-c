﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projet {
    static class Program {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // If application folder doesn't exists
            if(!Directory.Exists(Settings.APPDATAPATH)) {
                Directory.CreateDirectory(Settings.APPDATAPATH);
                if (MessageBox.Show(@"Mot de passe Administrateur: Truite.
Mot de passe Superviseur: Lapin.

Les fichiers de configuration et profils sont stockés dans le répertoire %appdata%/multiplikator2000.
Ouvrir le répertoire ?", "Premier lancement", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                    System.Diagnostics.Process.Start(Settings.APPDATAPATH);
                }
            }
            // If profile folder doesn't exists
            if (!Directory.Exists(Settings.PROFILESPATH)) {
                Directory.CreateDirectory(Settings.PROFILESPATH);
            }
            // If database doesn't exists
            if (!File.Exists(Settings.APPDATAPATH + "database.mkt")) {
                Database.createDatabase();
            }
            Tools.createDefaultUsersIfDontExists();

            Application.Run(new Multiplikator());
        }
    }
}
