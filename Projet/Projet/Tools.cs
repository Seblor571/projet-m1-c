﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Speech.Synthesis;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;

namespace Projet {
    /// <summary>
    /// Contient différents outils statiques
    /// </summary>
    abstract class Tools {

        /// <summary>
        /// Liste statique des profils
        /// </summary>
        public static List<Profile> LISTPROFILES = new List<Profile>();

        /// <summary>
        /// Random Number Generator : permet d'avoir des nombre aléatoires différents (quand générés à la même seconde)
        /// </summary>
        public static Random RND = new Random();

        /// <summary>
        /// Synthétiseur vocal : statique pour permettre de couper la phrase précédente
        /// </summary>
        public static SpeechSynthesizer synthesizer = new SpeechSynthesizer();

        /// <summary>
        /// Génère un nombre aléatoire entre un minimum et un maximum (inclus)
        /// </summary>
        /// <param name="min">Nombre minimum</param>
        /// <param name="max">Nombre maximum (inclut)</param>
        /// <returns></returns>
        public static int RNG(int min, int max) {
            return RND.Next(min, max + 1);
        }

        /// <summary>
        /// Coupe phrase en cours, et synthétise la phrase donnée en paramètre
        /// </summary>
        /// <param name="tts">Phrase à prononcer</param>
        public static void synthesize(string tts) {
            synthesizer.SpeakAsyncCancelAll();
            synthesizer.Volume = 100;  // 0...100
            // synthesizer.SelectVoiceByHints(VoiceGender.Male, VoiceAge.Senior)

            // Asynchronous
            synthesizer.SpeakAsync(tts);
        }

        /// <summary>
        /// Renvoie une nouvelle liste avec les éléments ordonnés aléatoirement
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <returns>Liste randomisée</returns>
        public static T[] randomizeArray<T>(T[] array) {
            Random rnd = new Random();
            T[] randomizedArray = array.OrderBy(x => rnd.Next()).ToArray();
            return randomizedArray;
        }

        /// <summary>
        /// Chiffre une chaine de caractère en SHA1
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string sha1Encode(string input) {

            SHA1 sha1Hash = SHA1.Create();

            byte[] data = sha1Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++) {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        /// <summary>
        /// Compare une chaine de caractère en clair à un hash SHA1
        /// </summary>
        /// <param name="input"></param>
        /// <param name="hash"></param>
        /// <returns></returns>
        public static bool sha1Verify(string input, string hash) {
            string hashOfInput = sha1Encode(input);

            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash)) {
                return true;
            }
            else {
                return false;
            }
        }

        /// <summary>
        /// Sérialise et sauvegarde l'objet donné au chemin spécifié (chemin du fichier avec nom du fichier)
        /// </summary>
        /// <typeparam name="T">Type de l'objet à enregistrer</typeparam>
        /// <param name="obj"></param>
        /// <param name="path"></param>
        public static void Save<T>(T obj, String path) {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            using (StreamWriter writer = new StreamWriter(path)) {
                serializer.Serialize(writer, obj);
            }
        }

        /// <summary>
        /// Charge tous les profiles dans la liste de Profile statique Tools.LISTPROFILES
        /// </summary>
        public static void loadProfiles() {
            Tools.LISTPROFILES.Clear();

            XmlSerializer serializer = new XmlSerializer(typeof(Profile));
            DirectoryInfo d = new DirectoryInfo(Settings.PROFILESPATH);

            // For each xml file in the profile directory
            foreach (FileInfo profilePath in d.GetFiles("*.xml")) {
                try {
                    StreamReader reader = new StreamReader(Settings.PROFILESPATH + profilePath);
                    Profile deser = (Profile)serializer.Deserialize(reader);

                    // Putting Admin & Supervisor first on the profiles list
                    if (deser._username == UserTypes.ADMINISTRATOR.getName()) {
                        Tools.LISTPROFILES.Insert(0, deser);
                    }
                    else if (deser._username == UserTypes.SUPERVISOR.getName()) {
                        Tools.LISTPROFILES.Insert(1, deser);
                    }
                    else {
                        Tools.LISTPROFILES.Add(deser);
                    }
                    reader.Close();
                }
                catch (Exception) {
                    // Only printing error (the user don't have to mind this)
                    Console.WriteLine("Error while reading file " + profilePath);
                }
            }
        }

        /// <summary>
        /// Tente de charger un profil externe
        /// </summary>
        /// <returns>True si profil ajouté</returns>
        public static bool loadExternalProfile() {

            string newFileName = "";

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Open Text File";
            dialog.Filter = "Fichiers XML|*.xml";
            dialog.InitialDirectory = @"/";
            if (dialog.ShowDialog() == DialogResult.OK) {
                newFileName = dialog.SafeFileName.Substring(0, dialog.SafeFileName.Length - 4);
                while (File.Exists(Settings.PROFILESPATH + newFileName + ".xml") || !Tools.validateFileName(newFileName + ".xml")) {

                    newFileName = Microsoft.VisualBasic.Interaction.InputBox(@"Un profil existe déjà pour ce nom d'utilisateur, ou le nom de fichier est invalide..
Veuillez en entrer un nouveau nom de profil (nom d'utilisateur)", "Nom de profil invalide", newFileName);

                    if (newFileName == "") {
                        loadExternalProfile();
                        return false;
                    }
                }
            }
            else {
                return false;
            }

            try {
                XmlSerializer serializer = new XmlSerializer(typeof(Profile));

                StreamReader reader = new StreamReader(dialog.FileName);
                Profile deser = (Profile)serializer.Deserialize(reader);
                reader.Close();

                deser._username = newFileName;
                deser.saveProfile();
            }
            catch (Exception e) {
                Console.WriteLine(e);

                MessageBox.Show("Ce fichier ne correspond pas à un fichier de profil valide pour cette application.", "Profil invalide", MessageBoxButtons.OK, MessageBoxIcon.Error);
                loadExternalProfile();
                return false;
            }

            return true;
        }

        /// <summary>
        /// Indique si un profil existe avec ce nom d'utilisateur
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static Boolean profileExists(string username) {
            return File.Exists(Settings.PROFILESPATH + username + ".xml");
        }

        /// <summary>
        /// Valide un nom de fichier
        /// </summary>
        /// <param name="fileName">nom de fichier à valider</param>
        /// <returns>Booléen : True si le nom de fichier est valide</returns>
        public static bool validateFileName(string fileName) {
            FileInfo fi = null;
            try {
                fi = new System.IO.FileInfo(fileName);
            }
            catch (ArgumentException) { }
            catch (PathTooLongException) { }
            catch (NotSupportedException) { }

            return !ReferenceEquals(fi, null);
        }

        /// <summary>
        /// Valide un nom d'utilisateur. Correspond à validateFileName(string fileName), car le nom d'utilisateur doit être valide selon les critères de nommage de fichier
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static bool validateUsername(string username) {
            return validateFileName(username);
        }

        /// <summary>
        /// Vérifie l'équivalence entre le mot de passe de l'utilisateur stocké dans le fichier et celui entré dans le formulaire
        /// </summary>
        /// <param name="selectedProfile"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static Boolean checkLogin(Profile selectedProfile, string password) {
            if (selectedProfile != null) {
                if (Tools.sha1Verify(password, selectedProfile._password)) {
                    Session.Instance.profile = selectedProfile;
                }
                else {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Crée les profile par défaut (Administrateur & Superviseur)
        /// </summary>
        public static void createDefaultUsersIfDontExists() {
            if (!File.Exists(Settings.PROFILESPATH + UserTypes.ADMINISTRATOR.getName() + ".xml")) {
                Profile admin = new Profile();
                admin._username = UserTypes.ADMINISTRATOR.getName();
                admin._password = Tools.sha1Encode("Truite");
                admin.saveProfile();
            }
            if (!File.Exists(Settings.PROFILESPATH + UserTypes.SUPERVISOR.getName() + ".xml")) {
                Profile sup = new Profile();
                sup._username = UserTypes.SUPERVISOR.getName();
                sup._password = Tools.sha1Encode("Lapin");
                sup.saveProfile();
            }
        }

        // ----- Math functions -----

        /// <summary>
        /// Ease in (acélération "douce")
        /// </summary>
        /// <param name="t">Temps</param>
        /// <param name="b">Valeur de départ</param>
        /// <param name="c">Changement de la valeur</param>
        /// <param name="d">Durée</param>
        /// <returns></returns>
        public static double easeInQuad(double t, double b, double c, double d) {
            t /= d;
            return c * t * t + b;
        }

        /// <summary>
        /// Ease out (décélération "douce")
        /// </summary>
        /// <param name="t">Temps</param>
        /// <param name="b">Valeur de départ</param>
        /// <param name="c">Changement de la valeur</param>
        /// <param name="d">Durée</param>
        /// <returns></returns>
        public static double easeOutQuad(double t, double b, double c, double d) {
            t /= d;
            return -c * t * (t - 2) + b;
        }

    }
}
