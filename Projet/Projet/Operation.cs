﻿using System;

namespace Projet {
    /// <summary>
    /// Opération de multiplication composée de 2 opérande (droite et gauche)
    /// </summary>
    public class Operation {

        private int _operandeGauche, _operandeDroite;
        private Levels _level;


        // ------- Constructor -------
        public Operation(int operandeGauche, int operandeDroite, Levels level) {
            if (Tools.RND.Next() % 2 == 0) {
                this._operandeDroite = operandeDroite;
                this._operandeGauche = operandeGauche;
            }
            else {
                this._operandeDroite = operandeGauche;
                this._operandeGauche = operandeDroite;
            }
            this._level = level;
        }

        // ------- Getters / Setters -------

        /// <summary>
        /// Opérande gauche de l'opération
        /// </summary>
        public int operandeGauche {
            get {
                return this._operandeGauche;
            }
            set {
                this._operandeGauche = value;
            }
        }

        /// <summary>
        /// Opérande droite de l'opération
        /// </summary>
        public int operandeDroite {
            get {
                return this._operandeDroite;
            }
            set {
                this._operandeDroite = value;
            }
        }

        /// <summary>
        /// Niveau de l'opération
        /// </summary>
        public Levels level {
            get {
                return this._level;
            }
        }

        /// <summary>
        /// Résultat de l'opération
        /// </summary>
        public int result {
            get {
                return this._operandeGauche * this._operandeDroite;
            }
        }


        // ------- Methodes -------

        /// <summary>
        /// Génère une opération pour le niveau donné
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public static Operation generateOperation(Levels level) {

            Operation op = new Operation(Tools.RNG(level.getMinLeft(), level.getMaxLeft()), Tools.RNG(level.getMinRight(), level.getMaxRight()), level);

            return op;
        }

        /// <summary>
        /// Fonction ToString qui renvoie une chaine de test (function de debug)
        /// </summary>
        /// <returns></returns>
        public override String ToString() {
            string result = "";
            result = "Operation: " + this._operandeGauche + "x" + this._operandeDroite + "=" + this.result;
            return result.Trim();
        }

        /// <summary>
        /// Renvoie une représentation de l'opération sans la solution
        /// </summary>
        /// <returns></returns>
        public String ToStringWithoutAnswer() {
            string result = "";
            result = this._operandeGauche + "x" + this._operandeDroite;
            return result.Trim();
        }

        /// <summary>
        /// Renvoie une représentation de l'opération avec la solution
        /// </summary>
        /// <returns></returns>
        public String ToStringWithAnswer() {
            string result = "";
            result = this._operandeGauche + "x" + this._operandeDroite + "=" + this.result;
            return result.Trim();
        }

        /// <summary>
        /// Generates list of operations
        /// </summary>
        /// <param name="level">Level of operations</param>
        /// <param name="transition">can be any number whose abselute value is < nbTransitionState</param>
        /// <returns>List of 20 Operations</returns>
        public static Operation[] generateAllOperations(Levels level, int transition = 0) {

            Operation[] ops = new Operation[20];

            int i = 0;
            // If transition is not out of bounds, generate transition operations
            if (!(level == Levels.LEVEL1_1 && transition < 0) && !(level == Levels.LEVEL3 && transition > 0)) {
                for (; i < (20 * Math.Abs(transition)) / (Settings.Instance.nbTransitionState + 1); i++) {
                    Operation ope = generateOperation(level + Math.Sign(transition));
                    while (Operation.listContains(ops, ope)) {
                        ope = generateOperation(level + Math.Sign(transition));
                        if (level == Levels.LEVEL1_3) {
                            // On oblige la validation des opérations du niveau 1_3 (car pas assez d'opérations possible)
                            break;
                        }
                    }
                    ops[i] = ope;
                }
            }

            // Generate the remaning operations
            for (; i < 20; i++) {
                Operation ope = generateOperation(level);
                while (Operation.listContains(ops, ope)) {
                    ope = generateOperation(level);
                    if (level == Levels.LEVEL1_3) {
                        // On oblige la validation des opérations du niveau 1_3 (car pas assez d'opérations possible)
                        break;
                    }
                }
                ops[i] = ope;
            }

            return Tools.randomizeArray<Operation>(ops);
        }

        /// <summary>
        /// Systhétise vocalement l'opération sans la réponse
        /// </summary>
        public void toSpeech() {
            Tools.synthesize(this._operandeGauche + " fois " + this._operandeDroite);
        }

        /// <summary>
        /// Systhétise vocalement l'opération avec la réponse
        /// </summary>
        public void toSpeechCorrect() {
            Tools.synthesize(this._operandeGauche + " fois " + this._operandeDroite + ", égale " + this.result);
        }

        /// <summary>
        /// Compare avec une autre opération si les opérandes sont identiques (qu'importe l'ordre)
        /// </summary>
        /// <param name="ope"></param>
        /// <returns></returns>
        public bool isSame(Operation ope) {
            if (ope == null) {
                return false;
            }
            return (this.operandeDroite == ope.operandeDroite && this.operandeGauche == ope.operandeGauche)
                || (this.operandeDroite == ope.operandeGauche && this.operandeGauche == ope.operandeDroite);
        }

        /// <summary>
        /// Renvoie true si la liste contient l'opération donnée
        /// </summary>
        /// <param name="ops"></param>
        /// <param name="ope"></param>
        /// <returns></returns>
        public static bool listContains(Operation[] ops, Operation ope) {
            foreach (Operation currentOpe in ops) {
                if (ope.isSame(currentOpe)) {
                    return true;
                }
            }
            return false;
        }

    }
}
