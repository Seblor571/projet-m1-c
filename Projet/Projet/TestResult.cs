﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet {
    /// <summary>
    /// Objet métier d'un résultat de test
    /// </summary>
    class TestResult {

        /// <summary>
        /// Date du résultat
        /// </summary>
        public DateTime date;

        /// <summary>
        /// Score de résultat
        /// </summary>
        public int score;

        /// <summary>
        /// Niveau de base du test
        /// </summary>
        public Levels level;

        /// <summary>
        /// Nombre d'opérations du niveau supérieur
        /// </summary>
        public int nbOpeSup;

        /// <summary>
        /// Nombre de succès parmis les opérations du niveau supérieur
        /// </summary>
        public int succOpeSup;

        /// <summary>
        /// Construit un résultat de test avec toutes les informations utiles
        /// </summary>
        /// <param name="date">Date du test</param>
        /// <param name="score">Score du test</param>
        /// <param name="level">Niveau de base du test</param>
        /// <param name="nbOpeSup">Nombre d'opérations du niveau supérieur</param>
        /// <param name="succOpeSup">Nombre de succès parmis les opérations du niveau supérieur</param>
        public TestResult(DateTime date, int score, Levels level, int nbOpeSup, int succOpeSup) {
            this.date = date;
            this.score = score;
            this.level = level;
            this.nbOpeSup = nbOpeSup;
            this.succOpeSup = succOpeSup;
        }

    }
}
