﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet {
    /// <summary>
    /// Item pour ComboBox listant les profils
    /// </summary>
    class ProfileComboBoxItem {

        /// <summary>
        /// Valeur de l'Item
        /// </summary>
        public Profile Value;

        /// <summary>
        /// Texte affiché dans la ComboBox
        /// </summary>
        public string Text;

        public ProfileComboBoxItem(Profile value, string display) {
            this.Value = value;
            this.Text = display;
        }

        public override string ToString() {
            return Text;
        }

    }
}
