var indexSectionsWithContent =
{
  0: "_acdefgilmnoprstuv",
  1: "acdlmopstu",
  2: "p",
  3: "acdlmoprstu",
  4: "acdegilmnoprstuv",
  5: "_acdlmnoprstv",
  6: "lu",
  7: "als",
  8: "adfiloprtv",
  9: "censt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Events"
};

